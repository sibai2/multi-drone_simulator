"""
This file contains a single function that verifies model
"""
import time

import pdb
import copy


def verify(data, simFunction, paramConfig={}):
    """
    Verification algorithm.
    It does the verification and print out the verify result.
    Args:
        data (dict): dictionary that contains params for the input file
        simFunction (function): black-box simulation function
        paramConfig (dict): user-specified configuration

    Returns:
        Safety (str): safety of the system
        Reach (obj): reach tube object

    """
    globall time = 0
    for d1 in range(len(drones)):
        full_tube[d1] = []
        for every segment in  d1s path
            cur_initialset_transformed[d1] = transform(intersection withh goalset, prev segment, cur segment)
            if tubecache contains cur_initialset_transformed[d1]
                get the reachtube, intersection of goal set withh the tube, min andd max time of reaching the goal set
                transform the tube to original coordinates
            else:
                compute the tube in original coordinates, intersection of goal set withh the tube, next initial set, min andd max time of reaching the goal set
                (the time array of the tube would still be in relative time)
                transform the tube to uniform coordinates and save it in tubecache
            
            add to the minimum and maximum time of every interval in the time array the globall time
        
            subtract fromm the min time the duration (max - min time of reaching the goal in the previous segment) in every rectangle in the tube (we are keeping the time list separate so we will change that instead of the polytopes)

            num_of_steps_behind = duration / timestep
            len_full_tube = len(full_tube[d1])
            for i in range(num_of_steps_behind):
                for j in range(num_of_steps_behind - i):
                    fj = len_full_tube - num_of_steps_behind + j
                    full_tube[d1][fj] = union full_tube[d1][fj] and cur_tube[d1][j]

            append cur_tube[num_of_steps_behind:] to full_tube
            globall time += end (relative) time of the tube

    for i in len(t):
        for d1 in range(len(drones)):
            for d2 in range(d1):
                check the intersection of full_tube[d1][i] and full_tube[d2][i]



                    
                
            
            

        



    global_time = 0
    track_dict = {}
    cur_segment = [0] * len(drones)
    cur_time = [0] * len(drones)
    initset_duration = [0] * len(drones)
    
    for initialset in initialsets:
        cur_initialset_transformed.append(transform(cur_initialset))
    while true:
        
        for d1 in range(drones):
            for d2 in range(drones):
            
            for i in range(lmax):
                for j in range(len(drones)):
                    if cur_time[j] <= global_time:

    
    for i in range(len(drones)):
        drone = drones[i]
        global_time = 0
        for segment in path:
            if tube is in tubecache:
                get the tube
            else:
                compute the tube in uniform coordinates
            check if the simulation used in the tube intersects the unsafe set.
            check if the tube intersects any of the unsafe sets (remember that the unsafe sets are in the original coordinates while the tube is in its corresponding coordinate system) This the intersection function should handle
            for j in range(i):
                comp_global_time = 0
                comp_drone = drones[j]
            
                
            global_time += tube.endtime



    # There are some fields can be config by user,
    # If user specified these fields in paramConfig, 
    # overload these parameters to userConfig
    overloadConfig(userConfig, paramConfig)
    
    translation_inv_v = 0 # 1 means the standard alg 2, 0 means simulating to the translation invariant variables
    
    
    
    
    tube_sym_label = []

    GLOBALREFINECOUNTER = 0

    params = parseVerificationInputFile(data)
    is_matrix = False
    # Build the graph object
    graph = buildGraph(
        params.vertex,
        params.edge,
        params.guards,
        params.resets
    )

    # Build the progress graph for jupyter notebook
    # isIpynb is used to detect if the code is running
    # on notebook or terminal, the graph will only be shown
    # in notebook mode
    progressGraph = Graph(params, isIpynb())

    # Make sure the initial mode is specfieid if the graph is dag
    # FIXME should move this part to input check
    # Bolun 02/12/2018
    assert graph.is_dag() == True or params.initialVertex != -1, "Graph is not DAG and you do not have initial mode!"
    
    print "params.linearGuard: ", params.linearGuard

    guard = Guard(params.variables,
                  params.linearGuard)  # the second argument is to choose the linear way in computing the next reachset after the intersection with the guard.
    n = len(params.variables)
    is_sym = False

    if "loadfile" in data:
        loadfile = data["loadfile"]
    else:
        loadfile = None
    if "savefile" in data:
        savefile = data["savefile"]
    else:
        savefile = None
    if 'transform' in data:
        is_matrix = True
        is_sym = True
        transform = Transform(np.array(ast.literal_eval(data['transform'])), n, is_matrix=True)
        checker = UniformChecker(params.unsafeSet, params.variables, varIdx=guard.varIdx, time_end=params.timeHorizon, is_sym=True)
    elif 'trans_invariant_vars' in data:
        is_sym = True
        checker = UniformChecker(params.unsafeSet, params.variables, varIdx=guard.varIdx,
                                 trans_inv_vars=ast.literal_eval(data['trans_invariant_vars']),
                                 is_sym=True, time_end=params.timeHorizon)
        transform = Transform(checker.trans_inv_indices, n)
    elif 'perm_invariant_vars' in data:
        is_sym = True
        checker = UniformChecker(params.unsafeSet, params.variables, varIdx=guard.varIdx,
                                 perm_inv_vars=ast.literal_eval(data['perm_invariant_vars']),
                                 is_sym=True, time_end=params.timeHorizon)
        transform = Transform(checker.perm_inv_indices, n, is_matrix=False, is_permutation=True)
    else:
        transform = None
        checker = UniformChecker(params.unsafeSet, params.variables, varIdx=guard.varIdx, time_end=params.timeHorizon)
    unsafe_dict = checker.unsafe_dict
    reseter = Reset(params.variables)
    startTime = time.time()
    # Step 1) Simulation Test
    # Random generate points, then simulate and check the result
    for _ in range(userConfig.SIMUTESTNUM):
        randInit = randomPoint(params.initialSet[0], params.initialSet[1])

        if DEBUG:
            print 'Random checking round ', _, 'at point ', randInit

        # Do a full hybrid simulation
        simResult = simulate(
            graph,
            randInit,
            params.timeHorizon,
            guard,
            simFunction,
            reseter,
            params.initialVertex,
            params.deterministic
        )

        # Check the traces for each mode
        for mode in simResult:
            safety = checker.checkSimuTrace(simResult[mode], mode)
            if safety == -1:
                print 'Current simulation is not safe. Program halt'
                print 'simulation time', time.time() - startTime
                return "UNSAFE", ReachTube([], None, [])
    simEndTime = time.time()

    # Step 2) Check Reach Tube
    # Calculate the over approximation of the reach tube and check the result
    print "Verification Begin"
    # Get the initial mode
    if params.initialVertex == -1:
        computeOrder = graph.topological_sorting(mode=OUT)
        initialVertex = computeOrder[0]
    else:
        initialVertex = params.initialVertex
    oldVertex = -1
    # Code for symmetries
    if loadfile is None:
        cache_forest_dict = defaultdict(list)
    else:
        start_load = time.time()
        with open(loadfile) as f:
            cache_forest_dict = pickle.load(f)
        print "load time:", time.time() - start_load
    # parent_stack_dict = defaultdict(list)
    result_dict = defaultdict(lambda: defaultdict(int))
    # Build the initial set stack
    curModeStack = InitialSetStack(initialVertex, userConfig.REFINETHRES, params.timeHorizon)
    curModeStack.stack.append(InitialSet(params.initialSet[0], params.initialSet[1]))
    curModeStack.bloatedTube.append(buildModeStr(graph, initialVertex))
    while True:
        # backwardFlag can be SAFE, UNSAFE or UNKNOWN
        # If the backwardFlag is SAFE/UNSAFE, means that the children nodes
        # of current nodes are all SAFE/UNSAFE. If one of the child node is
        # UNKNOWN, then the backwardFlag is UNKNOWN.
        backwardFlag = SAFE

        while curModeStack.stack:
            print "curModeStack.stack",str(curModeStack)
            print "curModeStack.stack[-1]: ", curModeStack.stack[-1]

            if not curModeStack.isValid():
                # A stack will be invalid if number of initial sets 
                # is more than refine threshold we set for each stack.
                # Thus we declare this stack is UNKNOWN
                print curModeStack.mode, "is not valid anymore"
                backwardFlag = UNKNOWN
                break

            # This is condition check to make sure the reach tube output file 
            # will be readable. Let me try to explain this.
            # A reachtube outout will be something like following
            # MODEA->MODEB
            # [0.0, 1.0, 1.1]
            # [0.1, 1.1, 1.2]
            # .....
            # Once we have refinement, we will add mutiple reach tube to 
            # this curModeStack.bloatedTube
            # However, we want to copy MODEA->MODEB so we know thats two different
            # reach tube from two different refined initial set
            # The result will be look like following
            # MODEA->MODEB
            # [0.0, 1.0, 1.1]
            # [0.1, 1.1, 1.2]
            # .....
            # MODEA->MODEB (this one gets copied!)
            # [0.0, 1.5, 1.6]
            # [0.1, 1.6, 1.7]
            # .....
            if isinstance(curModeStack.bloatedTube[-1], list):
                curModeStack.bloatedTube.append(curModeStack.bloatedTube[0])

            curStack = curModeStack.stack
            curVertex = curModeStack.mode
            curRemainTime = curModeStack.remainTime
            curLabel = graph.vs[curVertex]['label']
            print "curVertex:",curVertex
            cur_result_dict = result_dict[curLabel]
            cur_cache_forest = cache_forest_dict[curLabel]
            cur_unsafe_set = pc.union(unsafe_dict['Allmode'], unsafe_dict[curLabel])
            # cur_parent_stack = parent_stack_dict[curVertex]
            curSuccessors = graph.successors(curVertex)
            curInitial = [curStack[-1].lowerBound, curStack[-1].upperBound]
            # Update the progress graph
            progressGraph.update(buildModeStr(graph, curVertex), curModeStack.bloatedTube[0], curModeStack.remainTime)
            if curVertex != oldVertex:
                print "Mode changed From:", oldVertex, "To:", curVertex
                cur_cache_forest.append(CacheTree())
                cur_parent_stack = []
                oldVertex = curVertex
            if cur_cache_forest[-1].root is not None:
                assert len(cur_parent_stack) > 0, "error in cachetree node parent tracking, parent stack is empty!"
                parent = cur_parent_stack.pop()
                #print "parent initial states: ", pc.bounding_box(parent.initial_set)

            if len(curSuccessors) == 0:
                # If there is not successor
                # Calculate the current bloated tube without considering the guard
                curBloatedTube = None

                result = SymReturn.COMPUTE
                if is_sym and any(cache_tree.root is not None for cache_tree in cur_cache_forest):
                    # print "allmode unsafe:", unsafe_dict['Allmode']
                    # print "curmode unsafe:", unsafe_dict[curLabel]
                    # print "their union:", cur_unsafe_set
                    result, sim, curBloatedTube = sym_sim_reach(cur_unsafe_set,
                                                                transform,
                                                                cur_cache_forest,
                                                                curInitial)
                    print "in NO successors a result is added to the dictionary"
                    cur_result_dict[result] += 1
                if result is SymReturn.USELESS or result is SymReturn.COMPUTE:
                    if (is_sym or 'savefile' in data) and (transform is None or not transform.translation_inv or (translation_inv_v and transform.translation_inv)):
                        print "first choice"
                        curBloatedTube, traces = clacBloatedTube(
                            curLabel,
                            curInitial,
                            curRemainTime,
                            simFunction,
                            params.bloatingMethod,
                            params.kvalue,
                            userConfig.SIMTRACENUM
                        )
                        if cur_cache_forest[-1].root is None:
                            cur_node = TreeNode(pc.box2poly(np.column_stack(curInitial)), reachtube=curBloatedTube, traces=traces)
                            cur_cache_forest[-1].root = cur_node
                            print "adding root"
                        else:
                            cur_node = TreeNode(pc.box2poly(np.column_stack(curInitial)), reachtube=curBloatedTube, traces=traces, parent=parent)
                            parent.children.append(cur_node)
                    
                    elif is_sym and transform.translation_inv:
                        # if it is translation invariant, then compute the reachtube first with the translation invariant components being a single point in the initial set
                        # add it to the cacheTree and then call the symmetry code with the original initial set to get the reachtube. This is more efficient and accurate since the original one has intervals for these indices in the initial set which would increase the over-approximation error while if we have only a single point at these indices, then the over-approximation error because of these dimensions would be eliminated.
                        proj_curInitial = copy.deepcopy(curInitial)
                        print "curInitial: ", curInitial
                        for idx in transform.trans_inv_indices:
                            temp = proj_curInitial[0][idx-1] / 2.0 + proj_curInitial[1][idx-1] / 2.0
                            proj_curInitial[0][idx-1] = temp
                            proj_curInitial[1][idx-1] = temp
                        curBloatedTube_temp, traces = clacBloatedTube(
                                                                 curLabel,
                                                                 proj_curInitial,
                                                                 curRemainTime,
                                                                 simFunction,
                                                                 params.bloatingMethod,
                                                                 params.kvalue,
                                                                 userConfig.SIMTRACENUM
                                                                 )
                        #print "curBloatedTube: ", curBloatedTube

                        if cur_cache_forest[-1].root is None:
                            cur_node = TreeNode(pc.box2poly(np.column_stack(proj_curInitial)), reachtube=curBloatedTube_temp, traces=traces)
                            cur_cache_forest[-1].root = cur_node
                        else:
                            cur_node = TreeNode(pc.box2poly(np.column_stack(proj_curInitial)), reachtube=curBloatedTube_temp, traces=traces, parent=parent)
                            parent.children.append(cur_node)
                        #result, sim, curBloatedTube = sym_sim_reach(cur_unsafe_set,
                        #                                            transform,
                        #                                            cur_cache_forest,
                        #                                            curInitial)
                        curBloatedTube = copy.deepcopy(curBloatedTube_temp)
                        bloating_values = [[],[]]
                        for idx in transform.trans_inv_indices:
                            l = curInitial[0][idx-1] - proj_curInitial[0][idx-1]
                            u = curInitial[1][idx-1] - proj_curInitial[1][idx-1]
                            bloating_values[0].append(l)
                            bloating_values[1].append(u)
                    
                        for i in range(len(traces)):
                            trace = traces[i]
                            print "Checking the ", i, "th trace"
                            # trace and trace_tran have time, must be stripped for initial set compare
                            trace_tube = []
                            for j in range(trace.shape[0]):
                                lower = []
                                upper = []
                                for k in range(trace[j].shape[0]):
                                    # TODO: optimize by transforming trans_inv_indices to a set
                                    if k in transform.trans_inv_indices:
                                        lower.append(trace[j][k] + curInitial[0][k-1] - trace[0][k])
                                        upper.append(trace[j][k] + curInitial[1][k-1] - trace[0][k])
                                    else:
                                        lower.append(trace[j][k])
                                        upper.append(trace[j][k])
                                trace_tube.append(lower)
                                trace_tube.append(upper)
                            
                            #pdb.set_trace()
                                
                            if UNSAFE == checker.checkReachTube(trace_tube, curLabel) or UNKNOWN == checker.checkReachTube(trace_tube, curLabel):
                                print "Result is UNSAFE"
                                result = UNSAFE
                                break
                        
                        for i in range(0,len(curBloatedTube_temp),2):
                            for j in range(len(transform.trans_inv_indices)):
                                idx = transform.trans_inv_indices[j]
                                curBloatedTube[i][idx] = curBloatedTube_temp[i][idx] + bloating_values[0][j]
                                curBloatedTube[i+1][idx] = curBloatedTube_temp[i+1][idx] + bloating_values[1][j]
                                    
                        #print "curInitial_after: ", curInitial
                        #print "curBloatedTube: ", curBloatedTube
                        #if result is SymReturn.USELESS or result is SymReturn.COMPUTE:
                        #    pdb.set_trace()
                    else:
                        curBloatedTube, traces = clacBloatedTube(
                                                                 curLabel,
                                                                 curInitial,
                                                                 curRemainTime,
                                                                 simFunction,
                                                                 params.bloatingMethod,
                                                                 params.kvalue,
                                                                 userConfig.SIMTRACENUM
                                                                 )
                    



            candidateTube = []
            shortestTime = float("inf")
            shortestTube = None

            for curSuccessor in curSuccessors:
                #if 'transform' in data:
                #    raise ValueError
                edgeID = graph.get_eid(curVertex, curSuccessor)
                curGuardStr = graph.es[edgeID]['guards']
                curResetStr = graph.es[edgeID]['resets']
                # Calculate the current bloated tube with guard involved
                # Pre-check the simulation trace so we can get better bloated result
                result = SymReturn.USELESS
                if is_sym and any(cache_tree.root is not None for cache_tree in cur_cache_forest):
                    result, sim, curBloatedTube = sym_sim_reach(pc.union(unsafe_dict['Allmode'], unsafe_dict[curLabel], check_convex=True),
                                                                transform,
                                                                cur_cache_forest,
                                                                curInitial)
                    print "WITH successors a result is added to the dictionary"
                    cur_result_dict[result] += 1
                if result is SymReturn.USELESS or result is SymReturn.COMPUTE:
                    curBloatedTube, traces = clacBloatedTube(curLabel,
                                                             curInitial,
                                                             curRemainTime,
                                                             simFunction,
                                                             params.bloatingMethod,
                                                             params.kvalue,
                                                             userConfig.SIMTRACENUM
                                                             )
                    if cur_cache_forest[-1].root is None:
                        cur_node = TreeNode(pc.box2poly(np.column_stack(curInitial)), reachtube=curBloatedTube, traces=traces)
                        cur_cache_forest[-1].root = cur_node
                    else:
                        cur_node = TreeNode(pc.box2poly(np.column_stack(curInitial)), reachtube=curBloatedTube, traces=traces, parent=parent)
                        parent.children.append(cur_node)

                # Use the guard to calculate the next initial set
                nextInit, trunckedResult, transiteTime = guard.guardReachTube(
                    curBloatedTube,
                    curGuardStr,
                )

                if nextInit == None:
                    print "is_sym: ", is_sym
                    print "nextInit is empty"
                    continue

                # Reset the next initial set
                nextInit = reseter.resetSet(curResetStr, nextInit[0], nextInit[1])

                # Build next mode stack
                nextModeStack = InitialSetStack(
                    curSuccessor,
                    userConfig.CHILDREFINETHRES,
                    curRemainTime - transiteTime,
                )
                nextModeStack.parent = curModeStack
                nextModeStack.stack.append(InitialSet(nextInit[0], nextInit[1]))
                nextModeStack.bloatedTube.append(curModeStack.bloatedTube[0] + '->' + buildModeStr(graph, curSuccessor))
                curStack[-1].child[curSuccessor] = nextModeStack
                if len(trunckedResult) > len(candidateTube):
                    candidateTube = trunckedResult

                # In case of must transition
                # We need to record shortest tube
                # As shortest tube is the tube invoke transition
                if trunckedResult[-1][0] < shortestTime:
                    shortestTime = trunckedResult[-1][0]
                    shortestTube = trunckedResult

            # Handle must transition
            if params.deterministic and len(curStack[-1].child) > 0:
                nextModesInfo = []
                for nextMode in curStack[-1].child:
                    nextModesInfo.append((curStack[-1].child[nextMode].remainTime, nextMode))
                # This mode gets transit first, only keep this mode
                maxRemainTime, maxTimeMode = max(nextModesInfo)
                # Pop other modes because of deterministic system
                for _, nextMode in nextModesInfo:
                    if nextMode == maxTimeMode:
                        continue
                    curStack[-1].child.pop(nextMode)
                candidateTube = shortestTube
                print "Handle deterministic system, next mode", graph.vs[curStack[-1].child.keys()[0]]['label']

            if not candidateTube:
                candidateTube = curBloatedTube

            # Check the safety for current bloated tube
            if result is SymReturn.COMPUTE or result is SymReturn.USELESS:
                safety = checker.checkReachTube(candidateTube, curLabel)
                if safety == SAFE:
                    tube_sym_label.extend([0 for _ in range(len(candidateTube))])
                    # print candidateTube
                print "DRYVR_SAFE?", safety == SAFE
            elif result == SymReturn.SAFE:
                tube_sym_label.extend([1 for _ in range(len(candidateTube))])
                safety = SAFE
                assert SAFE == checker.checkReachTube(candidateTube, curLabel), "Bounding Box Issues"
            else:
                safety = UNSAFE
            if safety == UNSAFE:
                print "System is not safe in Mode ", curLabel
                # Start back Tracking from this point and print tube to a file
                # push current unsafeTube to unsafe tube holder
                unsafeTube = [curModeStack.bloatedTube[0]] + candidateTube
                while curModeStack.parent is not None:
                    prevModeStack = curModeStack.parent
                    unsafeTube = [prevModeStack.bloatedTube[0]] + prevModeStack.stack[-1].bloatedTube + unsafeTube
                    curModeStack = prevModeStack
                print 'simulation time', simEndTime - startTime
                print 'verification time', time.time() - simEndTime
                print 'refine time', GLOBALREFINECOUNTER
                writeReachTubeFile(unsafeTube, UNSAFEFILENAME)
                retReach = ReachTube(curModeStack.bloatedTube, params.variables, params.vertex)
                print "results:", cur_result_dict
                return "UNSAFE", retReach

            elif safety == UNKNOWN:
                # Refine the current initial set
                print curModeStack.mode, "check bloated tube unknown"
                discardInitial = curModeStack.stack.pop()
                if is_sym and transform.translation_inv and not translation_inv_v:
                    initOne, initTwo = discardInitial.refine(copy.deepcopy(transform.trans_inv_indices))  # we added the optional parameter of indices that the algorithm shouldn't refine on. This is needed for translation invariance for the variables that do not appear on the RHS since their refinement wouldn't affect accuracy of the reachtube computed since it is computed for initial sets where these variables are points not intervals.
                else:
                    print "2:first choice"
                    initOne, initTwo = discardInitial.refine()
                #initOne, initTwo = discardInitial.refine()
                if is_sym or 'savefile' in data:
                    cur_parent_stack.append(cur_node)
                    cur_parent_stack.append(cur_node)
                curModeStack.stack.append(initOne)
                curModeStack.stack.append(initTwo)
                GLOBALREFINECOUNTER += 1

            elif safety == SAFE:
                print "Mode", curModeStack.mode, "check bloated tube safe"
                if curModeStack.stack[-1].child:
                    curModeStack.stack[-1].bloatedTube += candidateTube
                    nextMode, nextModeStack = curModeStack.stack[-1].child.popitem()
                    curModeStack = nextModeStack
                    print "Child exist in cur mode inital", curModeStack.mode, "is curModeStack Now"
                else:
                    curModeStack.bloatedTube += candidateTube
                    curModeStack.stack.pop()
                    print "No child exist in current initial, pop"

        if curModeStack.parent is None:
            # We are at head now
            if backwardFlag == SAFE:
                # All the nodes are safe
                print "System is Safe!"
                print "refine time", GLOBALREFINECOUNTER
                print 'simulation time', simEndTime - startTime
                print 'verification time', time.time() - simEndTime
                print  "results:", cur_result_dict
                # print "tube_sym_label: ", tube_sym_label
                writeReachTubeSymType(tube_sym_label,"output/reachtube_symlabel.txt" )
                writeReachTubeFile(curModeStack.bloatedTube, REACHTUBEOUTPUT)
                if savefile is not None:
                    with open(str(savefile), 'w+') as f:
                        pickle.dump(cache_forest_dict, f)
                retReach = ReachTube(curModeStack.bloatedTube, params.variables, params.vertex)
                return "SAFE", retReach
            elif backwardFlag == UNKNOWN:
                print "Hit refine threshold, system halt, result unknown"
                print 'simulation time', simEndTime - startTime
                print 'verification time', time.time() - simEndTime
                print "results:", cur_result_dict
                return "UNKNOWN", None
        else:
            if backwardFlag == SAFE:
                prevModeStack = curModeStack.parent
                prevModeStack.stack[-1].bloatedTube += curModeStack.bloatedTube
                print 'back flag safe from', curModeStack.mode, 'to', prevModeStack.mode
                if len(prevModeStack.stack[-1].child) == 0:
                    # There is no next mode from this initial set
                    prevModeStack.bloatedTube += prevModeStack.stack[-1].bloatedTube
                    prevModeStack.stack.pop()
                    curModeStack = prevModeStack
                    print "No child in prev mode initial, pop,", prevModeStack.mode, "is curModeStack Now"
                else:
                    # There is another mode transition from this initial set
                    nextMode, nextModeStack = prevModeStack.stack[-1].child.popitem()
                    curModeStack = nextModeStack
                    print "Child exist in prev mode inital", nextModeStack.mode, "is curModeStack Now"
            elif backwardFlag == UNKNOWN:
                prevModeStack = curModeStack.parent
                print 'back flag unknown from', curModeStack.mode, 'to', prevModeStack.mode
                discardInitial = prevModeStack.stack.pop()
                initOne, initTwo = discardInitial.refine()
                prevModeStack.stack.append(initOne)
                prevModeStack.stack.append(initTwo)
                curModeStack = prevModeStack
                GLOBALREFINECOUNTER += 1


def graphSearch(data, simFunction, paramConfig={}):
    """
    DryVR controller synthesis algorithm.
    It does the controller synthesis and print out the search result.
    tube and transition graph will be stored in ouput folder if algorithm finds one
    
    Args:
        data (dict): dictionary that contains params for the input file
        simFunction (function): black-box simulation function

    Returns:
        None

    """
    # There are some fields can be config by user,
    # If user specified these fields in paramConfig, 
    # overload these parameters to userConfig
    overloadConfig(userConfig, paramConfig)
    # Parse the input json file and read out the parameters
    params = parseRrtInputFile(data)
    # Construct objects
    checker = UniformChecker(params.unsafeSet, params.variables)
    goalSetChecker = GoalChecker(params.goalSet, params.variables)
    distanceChecker = DistChecker(params.goal, params.variables)
    # Read the important param
    availableModes = params.modes
    startModes = params.modes
    remainTime = params.timeHorizon
    minTimeThres = params.minTimeThres

    # Set goal rach flag to False
    # Once the flag is set to True, It means we find a transition Graph
    goalReached = False

    # Build the initial mode stack
    # Current Method is ugly, we need to get rid of the initial Mode for GraphSearch
    # It helps us to achieve the full automate search
    # TODO Get rid of the initial Mode thing
    random.shuffle(startModes)
    dummyNode = GraphSearchNode("start", remainTime, minTimeThres, 0)
    for mode in startModes:
        dummyNode.children[mode] = GraphSearchNode(mode, remainTime, minTimeThres, dummyNode.level + 1)
        dummyNode.children[mode].parent = dummyNode
        dummyNode.children[mode].initial = (params.initialSet[0], params.initialSet[1])

    curModeStack = dummyNode.children[startModes[0]]
    dummyNode.visited.add(startModes[0])

    startTime = time.time()
    while True:

        if not curModeStack:
            break

        if curModeStack == dummyNode:
            startModes.pop(0)
            if len(startModes) == 0:
                break

            curModeStack = dummyNode.children[startModes[0]]
            dummyNode.visited.add(startModes[0])
            continue

        print "curModeStack:", str(curModeStack)

        # Keep check the remain time, if the remain time is less than minTime
        # It means it is impossible to stay in one mode more than minTime
        # Therefore, we have to go back to parents
        if curModeStack.remainTime < minTimeThres:
            print "Back to previous mode because we cannot stay longer than the min time thres"
            curModeStack = curModeStack.parent
            continue

        # If we have visited all available modes
        # We should select a new candidate point to proceed
        # If there is no candidates available,
        # Then we can say current node is not valid and go back to parent
        if len(curModeStack.visited) == len(availableModes):
            if len(curModeStack.candidates) < 2:
                print "Back to previous mode because we do not have any other modes to pick"
                curModeStack = curModeStack.parent
                # If the tried all possible cases with no luck to find path
                if not curModeStack:
                    break
                continue
            else:
                print "Pick a new point from candidates"
                curModeStack.candidates.pop(0)
                curModeStack.visited = set()
                curModeStack.children = {}
                continue

        # Generate bloated tube if we haven't done so
        if not curModeStack.bloatedTube:
            print "no bloated tube find in this mode, generate one"
            curBloatedTube = clacBloatedTube(
                curModeStack.mode,
                curModeStack.initial,
                curModeStack.remainTime,
                simFunction,
                params.bloatingMethod,
                params.kvalue,
                userConfig.SIMTRACENUM
            )

            # Cut the bloated tube once it intersect with the unsafe set
            curBloatedTube = checker.cutTubeTillUnsafe(curBloatedTube)

            # If the tube time horizon is less than minTime, it means
            # we cannot stay in this mode for min thres time, back to the parent node
            if not curBloatedTube or curBloatedTube[-1][0] < minTimeThres:
                print "bloated tube is not long enough, discard the mode"
                curModeStack = curModeStack.parent
                continue
            curModeStack.bloatedTube = curBloatedTube

            # Generate candidates points for next node
            randomSections = curModeStack.randomPicker(userConfig.RANDSECTIONNUM)

            if not randomSections:
                print "bloated tube is not long enough, discard the mode"
                curModeStack = curModeStack.parent
                continue

            # Sort random points based on the distance to the goal set
            randomSections.sort(key=lambda x: distanceChecker.calcDistance(x[0], x[1]))
            curModeStack.candidates = randomSections
            print "Generate new bloated tube and candidate, with candidates length", len(curModeStack.candidates)

            # Check if the current tube reaches goal
            result, tube = goalSetChecker.goalReachTube(curBloatedTube)
            if result:
                curModeStack.bloatedTube = tube
                goalReached = True
                break

        # We have visited all next mode we have, generate some thing new
        # This is actually not necssary, just shuffle all modes would be enough
        # There should not be RANDMODENUM things since it does not make any difference
        # Anyway, for each candidate point, we will try to visit all modes eventually
        # Therefore, using RANDMODENUM to get some random modes visit first is useless
        # TODO, fix this part
        if len(curModeStack.visited) == len(curModeStack.children):
            # leftMode = set(availableModes) - set(curModeStack.children.keys())
            # randomModes = random.sample(leftMode, min(len(leftMode), RANDMODENUM))
            # random.shuffle(randomModes)
            randomModes = availableModes
            random.shuffle(randomModes)

            randomSections = curModeStack.randomPicker(userConfig.RANDSECTIONNUM)
            for mode in randomModes:
                candidate = curModeStack.candidates[0]
                curModeStack.children[mode] = GraphSearchNode(mode, curModeStack.remainTime - candidate[1][0],
                                                              minTimeThres, curModeStack.level + 1)
                curModeStack.children[mode].initial = (candidate[0][1:], candidate[1][1:])
                curModeStack.children[mode].parent = curModeStack

        # Random visit a candidate that is not visited before
        for key in curModeStack.children:
            if not key in curModeStack.visited:
                break

        print "transit point is", curModeStack.candidates[0]
        curModeStack.visited.add(key)
        curModeStack = curModeStack.children[key]

    # Back track to print out trace
    print "RRT run time", time.time() - startTime
    if goalReached:
        print("goal reached")
        traces = []
        modes = []
        while curModeStack:
            modes.append(curModeStack.mode)
            if not curModeStack.candidates:
                traces.append([t for t in curModeStack.bloatedTube])
            else:
                # Cut the trace till candidate
                temp = []
                for t in curModeStack.bloatedTube:
                    if t == curModeStack.candidates[0][0]:
                        temp.append(curModeStack.candidates[0][0])
                        temp.append(curModeStack.candidates[0][1])
                        break
                    else:
                        temp.append(t)
                traces.append(temp)
            if curModeStack.parent != dummyNode:
                curModeStack = curModeStack.parent
            else:
                break
        # Reorganize the content in modes list for plotter use
        modes = modes[::-1]
        traces = traces[::-1]
        buildRrtGraph(modes, traces, isIpynb())
        for i in range(1, len(modes)):
            modes[i] = modes[i - 1] + '->' + modes[i]

        writeRrtResultFile(modes, traces, RRTOUTPUT)
    else:
        print("could not find graph")
