# MobileRobot
from scipy.integrate import odeint
import numpy as np
import math
from typing import Optional, List, Tuple


def position(q: np.array, t: float, vc: float, w: float,x_n, y_n, k) -> np.array:
	x = q[0]
	y = q[1]
	theta = q[2]

	v = vc

	x_ref = x_n
	y_ref = y_n
	L = k

	alpha = math.atan2((y_ref - y),(x_ref - x)) - theta
	dthetadt = 2*v*math.sin(alpha)/L

	dxdt = v*np.cos(theta)
	dydt = v*np.sin(theta)

	return np.array((dxdt, dydt, dthetadt))


# function to provide traces of the system
def TC_Simulate(mode: str, mode_parameters: Optional[List[float]], time_bound: float, time_step: float,
                initial_point: np.array) -> np.array:
    if mode == 'follow_waypoint':
        # mode parameters for this is the waypoint center
        t = np.arange(0, time_bound + time_step, time_step)
        assert isinstance(mode_parameters, list) and (len(mode_parameters) == 2) and (isinstance(
            mode_parameters[0], float) or isinstance(
            mode_parameters[0], int)), "must give length 2 list as params to follow_waypoint mode of Mobile Robot"
        red_args = (1, 0, mode_parameters[0], mode_parameters[1], 1)
        sol = odeint(position, initial_point, t, args=red_args, hmax=time_step)
        sol = [[x[0],x[1],x[2] % (2 * math.pi)] for x in sol]
        new_sol = []
        for x in sol:
            while x[2] > math.pi:
                x[2] = x[2] - 2 * math.pi
            while x[2] < -math.pi:
                x[2] = x[2] + 2 * math.pi
            new_sol.append(x)
        trace = np.column_stack((t, sol))
        return trace
    else:
        raise ValueError("Mode: ", mode, "is not defined for the Mobile Robot")


if __name__ == '__main__':
    trace = TC_Simulate("follow_waypoint", [3,3], 10, 0.1, [0,0,0])
    print("trace: ", trace)
