import numpy as np
import polytope as pc
from src.PolyUtils import PolyUtils
from src.ReachtubeSegment import ReachtubeSegment
from src.Agent import Agent, DRONE_TYPE
from typing import List, Dict, Tuple
from scipy.spatial import ConvexHull
import matplotlib

matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle, Polygon

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np
from itertools import product, combinations
import mpl_toolkits.mplot3d as plt3d
import itertools
from mayavi import mlab
from src.Agent import UniformTubeset


class Plotter:

    # TODO move away from static plotting and move towards OO plotting configurations
    @staticmethod
    def verts_to_tri():
        tri = np.row_stack([(0, 1, 2), (1, 2, 3), (0, 1, 4), (1, 4, 5), (0, 2, 4), (2, 4, 6), (3, 5, 7), (1, 3, 5),
                            (3, 6, 7), (2, 3, 6), (5, 6, 7), (4, 5, 6)])
        return tri

    @staticmethod
    def hrect_to_tri(hrect):
        x = [hrect[0][0]] * 4 + [hrect[1][0]] * 4
        y = [hrect[0][1]] * 2 + [hrect[1][1]] * 2
        y *= 2
        z = [hrect[0][2]] + [hrect[1][2]]
        z *= 4
        return Plotter.verts_to_tri(), x, y, z

    @staticmethod
    def cube_faces(xmin, xmax, ymin, ymax, zmin, zmax):
        faces = []

        x, y = np.mgrid[xmin:xmax:3j, ymin:ymax:3j]
        z = np.ones(y.shape) * zmin
        faces.append((x, y, z))

        x, y = np.mgrid[xmin:xmax:3j, ymin:ymax:3j]
        z = np.ones(y.shape) * zmax
        faces.append((x, y, z))

        x, z = np.mgrid[xmin:xmax:3j, zmin:zmax:3j]
        y = np.ones(z.shape) * ymin
        faces.append((x, y, z))

        x, z = np.mgrid[xmin:xmax:3j, zmin:zmax:3j]
        y = np.ones(z.shape) * ymax
        faces.append((x, y, z))

        y, z = np.mgrid[ymin:ymax:3j, zmin:zmax:3j]
        x = np.ones(z.shape) * xmin
        faces.append((x, y, z))

        y, z = np.mgrid[ymin:ymax:3j, zmin:zmax:3j]
        x = np.ones(z.shape) * xmax
        faces.append((x, y, z))

        return faces

    @staticmethod
    def mlab_plt_cube(xmin, xmax, ymin, ymax, zmin, zmax):
        faces = Plotter.cube_faces(xmin, xmax, ymin, ymax, zmin, zmax)
        for grid in faces:
            x, y, z = grid
            mlab.mesh(x, y, z, opacity=0.4)

    @staticmethod
    @mlab.show
    def plot(drone_num, tubes: List[List[ReachtubeSegment]], agents_list: List[Agent], is_unified: bool, unsafe_set,
             plot_step=1):
        # agents_tubes = [[segment.tube for segment in segment_list] for segment_list in tubes]
        colors = ['b', 'g', 'y']
        print("hello")

        # fig = plt.figure()
        # ax = fig.gca(projection='3d')
        # ax.set_aspect("equal")
        print("hello2")
        waypointslists = [[waypoint.mode_parameters for waypoint in agent.path] for agent in agents_list]
        legend_drone_rect = []
        legend_unsafe_set = []

        for d in range(drone_num):
            curr_agent = agents_list[d]
            color = colors[d]
            print("plotting1")
            if not is_unified:
                wp = np.array(waypointslists[d])
                if d == 0:
                    print("plotting2")
                    #plt.scatter(wp[:, 0], wp[:, 1], 'k' + 'o', label='waypoints')
                    mlab.plot3d(wp[:, 0], wp[:, 1], wp[:, 2])
                    #ax.add_line(line)
                    #ax.scatter(wp[:, 0], wp[:, 1], wp[:, 2], 'k')
                    # plt.scatter(wp[:, 0], wp[:, 1], 'k')
                else:
                    #plt.scatter(wp[:, 0], wp[:, 1], 'k' + 'o')
                    mlab.plot3d(wp[:, 0], wp[:, 1], wp[:, 2])
                    #ax.add_line(line)
                    #ax.scatter(wp[:, 0], wp[:, 1], wp[:, 2], 'k')
            curr_tubes = tubes[d]
            for ci in range(len(curr_tubes)):
                curr_segment = curr_tubes[ci]
                curr_tube = curr_segment.tube
                traces = curr_segment.trace
                for i in range(0, len(curr_tube), plot_step):
                    box_of_poly = PolyUtils.get_bounding_box(curr_tube[i])
                    Plotter.mlab_plt_cube(box_of_poly[0, 0], box_of_poly[1, 0], box_of_poly[0, 1], box_of_poly[1, 1], box_of_poly[0, 2], box_of_poly[1, 2])
            '''
            tri = None
            x = []
            y = []
            z = []
            # print("boundedboxed final tube:")
            for ci in range(len(curr_tubes)):
                curr_segment = curr_tubes[ci]
                curr_tube = curr_segment.tube
                traces = curr_segment.trace
                for i in range(0, len(curr_tube), plot_step):
                    box_of_poly = PolyUtils.get_bounding_box(curr_tube[i])
                    _triangles, _x, _y, _z = Plotter.hrect_to_tri(box_of_poly)
                    if tri is None:
                        tri = _triangles
                    else:
                        tri = np.row_stack((tri, _triangles))
                    z.extend(_z)
                    x.extend(_x)
                    y.extend(_y)

                for i in range(0, len(curr_tube), plot_step):
                    box_of_poly = PolyUtils.get_bounding_box(curr_tube[i])

                    rect = Rectangle(box_of_poly[0, 0:2], box_of_poly[1, 0] - box_of_poly[0, 0],
                                     box_of_poly[1, 1] - box_of_poly[0, 1], linewidth=1,
                                     edgecolor=color, facecolor='none')
                    if ci == 0 and i == 0:
                        legend_drone_rect.append(rect)
                    ax.add_patch(rect)
                '''
            #triplot = mlab.triangular_mesh(x, y, z, list(tri), colormap='summer')
        # color = 'r'
        '''
        if isinstance(unsafe_set, np.ndarray):
            unsafe_set = pc.box2poly(unsafe_set.T)
        if isinstance(unsafe_set, pc.Polytope):
            unsafe_set = [unsafe_set]
        for i in range(len(unsafe_set)):
            poly = unsafe_set[i]
            points = pc.extreme(poly)
            points = points[:, :2]
            hull = ConvexHull(points)
            poly_patch = Polygon(points[hull.vertices, :], alpha=.5, color=color, fill=True)
            if i == 0:
                legend_unsafe_set.append(poly_patch)
            ax.add_patch(poly_patch)
        '''
        # plt.legend((legend_drone_rect[0], legend_drone_rect[1], legend_unsafe_set[0]), ("first drone tube", "second drone tube", "unsafe set"))
        '''plt.ylim([-10, 15])
        plt.xlim([-5, 25])
        plt.tick_params(axis='both', which='major', labelsize=30)
        '''
        mlab.xlabel('x')
        mlab.ylabel('y')
        # plt.xlabel('x', fontsize=30)
        # plt.ylabel('y', fontsize=30)
        # plt.zlabel('z', fontsize=30)
        # plt
        # plt.show()
        mlab.zlabel('z')
        mlab.show()
        # return  l
