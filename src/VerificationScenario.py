import json
import os
from src.Waypoint import Waypoint
from src.Agent import Agent, TubeMaster, DRONE_TYPE
from src.Plot import Plotter
from src.ReachtubeSegment import ReachtubeSegment
import numpy as np
from typing import List, Dict, Optional, Tuple
import polytope as pc
from src.PolyUtils import PolyUtils
import math
import cProfile
import time
import pdb
from src.ParseModel import importFunctions
import subprocess
from src.dryvr_parser import dryvr_parse
from mayavi import mlab
import os
import sys


class VerificationScenario:

    def __init__(self, json_input_file: str, json_output_file: str = None) -> None:
        try:
            with open(json_input_file) as json_input:
                json_result = json.load(json_input)
        except FileNotFoundError:
            print("Json Scenario File", json_input_file, "not found")

        self.json_output_file = json_output_file
        agents_info_list = json_result["agents"]
        self.num_agents: int = len(agents_info_list)
        self.time_step: float = json_result["time_step"]
        self.reachability_engine: str = json_result["reachability_engine"]
        # TODO make these agent ID's Dynamically mapped with a hash of the dynamics summary
        self.agent_dynamics_ids: List[str] = [agent_info["directory"] for agent_info in agents_info_list]
        # list of polytopes one for each agent.
        initset_list = [agent_info["initialSet"][1] for agent_info in agents_info_list]
        self.agents_variables_list = [agent_info["variables"] for agent_info in agents_info_list]
        self.initsets: List[pc.Polytope] = [pc.box2poly(np.array(initset).T) for initset in initset_list]
        # TODO add support for controller level grid resolutions
        self.grid_resolution: np.array = np.array(json_result["grid_resolution"])
        # TODO Add waypoint IO

        self.agents_list: List[Agent]
        self.trans_guards_union: pc.Region = pc.Region(list_poly=[])
        # self.agents_list, self.trans_guards_union, self.trans_info_list =
        # VerificationScenario.get_agent_list_from_info( agents_info_list)
        self.agents_list, self.abs_agent_initset, self.abs_timebound, self.abs_edges_guards, self.abs_edges_guards_union,\
        self.abs_reach, self.abs_initset, self.abs_nodes_neighbors = \
            VerificationScenario.get_agent_list_from_info(agents_info_list, snowflake_path=True)

        self.max_dryvr_time_horizon = 10000


        # VerificationScenario.get_agent_list_from_info_snowflake(agents_info_list)
        # VerificationScenario.get_agent_list_from_info_snowflake(agents_info_list)
        #  VerificationScenario.get_agent_list_from_info(agents_info_list)
        # self.trans_guards_bbox_list = []
        # for i in range(len(self.trans_guards_union.list_poly)):
        #    self.trans_guards_bbox_list.append(PolyUtils.get_bounding_box(self.trans_guards_union.list_poly[i]))
        # print("length of trans_guards_bbox_list: ", len(self.trans_guards_bbox_list))
        # self.tube_master: TubeMaster = TubeMaster(self.grid_resolution, {key: self.reachability_engine for key
        #                                                                 in set(self.agent_dynamics_ids)},
        #                                          self.trans_guards_bbox_list, self.trans_info_list)

        if not json_output_file is None:
            self.reset_to_vir_reset_str_fn = importFunctions(agents_info_list[0]["directory"])[8] # TODO: change when more than one agent

        # dryvr_plotter_command = "python " + dryvr_path + "retrieve_dryvr_tube.py " + dryvr_path + "output/reachtube.txt"

        # self.json_output.write
        # self.json_output.write(str(json.dumps({"vertex": dryvr_vertex_list,
        #                                       "edge": dryvr_edge_list,
        #                                       "variables": dryvr_variables_list,
        #                                       "guards": dryvr_guards_list,
        #                                       "resets": dryvr_resets_list,
        #                                       "initialSet": dryvr_initset_list,
        #                                       "unsafeSet": dryvr_unsafeset_string,
        #                                       "timeHorizon": dryvr_timehorizon,
        #                                       "directory": dryvr_directory_string})))


        if "symmetry_level" in json_result and json_result["symmetry_level"].lower() == "0":
            sym_level = 0
            print("Warning, not using symmetry")
        elif "symmetry_level" in json_result and json_result["symmetry_level"].lower() == "1":
            sym_level = 1
            print("Warning, symmetry is used WITHOUT fixed point checking")
        else:
            sym_level = 2
            print("Great, symmetry is used WITH fixed point checking")
        for virtual_edge in self.abs_edges_guards_union:
            self.abs_edges_guards_union[virtual_edge] = \
                PolyUtils.get_region_bounding_box(self.abs_edges_guards_union[virtual_edge])
            print("virtual edge: ", virtual_edge)
            print("abs_edges_guards_union: ", self.abs_edges_guards_union[virtual_edge])
        self.tube_master: TubeMaster = TubeMaster(self.grid_resolution, {key: self.reachability_engine for key
                                                                         in set(self.agent_dynamics_ids)},
                                                  self.abs_edges_guards, self.abs_edges_guards_union, self.abs_reach, self.abs_initset,
                                                  self.abs_nodes_neighbors, sym_level)
        # TODO add support for Unsafe Set Class
        self.unsafe_set: List[pc.Polytope] = [pc.box2poly(np.array(json_result["unsafeSet"][i][1]).T) for i in
                                              range(len(json_result["unsafeSet"]))]
        self.agents_full_tubes: List[List[ReachtubeSegment]] = []
        self.reached_fixed_point = False
        # self.f = open(json_input_file[:-5] + ".txt", "w+")

    def construct_dryvr_input_file(self, dryvr_time_bound):
        dryvr_vertex_list = []
        dryvr_edge_list = []
        dryvr_guards_list = []
        dryvr_resets_list = []
        dryvr_unsafeset_string = ""
        dryvr_directory_string = "examples/Linear3D" # self.dryvr_path +
        dryvr_mode_to_id_dict = {}
        dryvr_mode_counter = 0

        for virtual_mode in self.abs_initset:
            dryvr_mode = str(virtual_mode)
            dryvr_mode = dryvr_mode.replace(",", ";")
            self.dryvrmode_to_virtualmode[dryvr_mode + "," + str(dryvr_mode_counter)] = virtual_mode
            dryvr_vertex_list.append(dryvr_mode)
            dryvr_mode_to_id_dict[virtual_mode] = dryvr_mode_counter
            dryvr_mode_counter += 1
        dryvr_variables_list = self.agents_variables_list[0]  ## change when we have more than one agent

        for virtual_edge in self.abs_edges_guards:
            first_mode = virtual_edge[0]
            second_mode = virtual_edge[1]
            first_mode_id = dryvr_mode_to_id_dict[first_mode]
            second_mode_id = dryvr_mode_to_id_dict[second_mode]
            dryvr_edge_list.append([first_mode_id, second_mode_id])
            guard_rect = PolyUtils.get_region_bounding_box(self.abs_edges_guards_union[virtual_edge])
            guard_string = "And("
            for i in range(len(dryvr_variables_list)):
                guard_string += "-1 * " + dryvr_variables_list[i] + "<=" + str(-1 * guard_rect[0][i]) + "," \
                                + dryvr_variables_list[i] + "<=" + str(guard_rect[1][i]) + ","
            guard_string += "t<=" + str(self.abs_timebound[virtual_edge]) + ",-t<=0.0"
            guard_string = guard_string + ")"
            state_dim = len(dryvr_variables_list)
            print("resets transform information: ", self.abs_edges_guards[virtual_edge][0][1], self.abs_edges_guards[virtual_edge][0][2])
            reset_string = self.reset_to_vir_reset_str_fn(np.array([0] * state_dim),
                                                          # TODO: remove this as it's not used
                                                          self.abs_edges_guards[virtual_edge][0][1],
                                                          # get the first element of the guard list transform information
                                                          self.abs_edges_guards[virtual_edge][0][2])
            dryvr_resets_list.append(reset_string)
            dryvr_guards_list.append(guard_string)

        dryvr_initset_list = self.abs_agent_initset[-1].tolist()
        dryvr_timehorizon = 0  # TODO: should be changed
        if self.max_dryvr_time_horizon == 10000:
            for virtual_edge in self.abs_timebound:
                dryvr_timehorizon = dryvr_timehorizon + self.abs_timebound[virtual_edge]
            self.max_dryvr_time_horizon = dryvr_timehorizon

        if dryvr_time_bound < self.max_dryvr_time_horizon:
            dryvr_timehorizon = dryvr_time_bound
        else:
            dryvr_timehorizon = self.max_dryvr_time_horizon


        # self.dryvr_path + "input/nondaginput/" +
        try:
            self.json_output = open(self.dryvr_path + "input/nondaginput/" + self.json_output_file, "w")
        except IOError:
            print('File does not exist')

        json.dump({"vertex": dryvr_vertex_list,
                   "edge": dryvr_edge_list,
                   "variables": dryvr_variables_list,
                   "guards": dryvr_guards_list,
                   "resets": dryvr_resets_list,
                   "initialSet": dryvr_initset_list,
                   "unsafeSet": dryvr_unsafeset_string,
                   "timeHorizon": dryvr_timehorizon,
                   "directory": dryvr_directory_string,
                   "initialVertex": 0,
                   "seed": 664891646,
                   "bloatingMethod": "PW",
                   "kvalue": [1] * len(dryvr_variables_list),
                   }, self.json_output, indent=2)
        self.json_output.close()

    def run_dryvr(self):
        cur_folder_path = os.getcwd()
        os.chdir(self.dryvr_path)
        # dryvr_call_command = "python " + "main.py " + "input/nondaginput/" + self.json_output_file
        # print("dryvr_call_command: ", dryvr_call_command)
        # process = subprocess.Popen(dryvr_call_command.split(), stdout=subprocess.PIPE)
        # output, error = process.communicate()
        params = ["python", "main.py", "input/nondaginput/" + self.json_output_file]
        # params = ["./dryvr_call_script.sh"]
        # params = ["pwd"]

        result = str(subprocess.check_output(params)).split('\\n')
        os.chdir(cur_folder_path)
        print("dryvr output: ", result)

    def get_dryvr_tube(self, node):
        #	fig1 = plt.figure()
        #	ax1 = fig1.add_subplot('111')
        lowerBound = []
        upperBound = []
        tube = []
        for key in sorted(node.lowerBound):
            lowerBound.append(node.lowerBound[key])
        for key in sorted(node.upperBound):
            upperBound.append(node.upperBound[key])

        for i in range(min(len(lowerBound), len(upperBound))):
            lb = list(map(float, lowerBound[i]))
            lb.pop(0)
            ub = list(map(float, upperBound[i]))
            ub.pop(0)  # TODO: removing time for now
            if len(lb) != len(self.agents_variables_list[0]) or len(ub) != len(self.agents_variables_list[0]):
                continue
            tube.append(np.array([lb, ub]))
            # tube.append(ub)

        return tube

    def parse_dryvr_output(self):
        try:
            file = open(self.dryvr_path + "output/reachtube.txt", 'r')
        except IOError:
            print('File does not exist')
        lines = file.readlines()
        initNode, y_min, y_max = dryvr_parse(lines)

        # ydim = eval(args.y)
        # xdim = eval(args.x)
        # Using DFS algorithm to Draw image per Node
        stack = [initNode]
        # initNode.printTube()
        while stack:
            curNode = stack.pop()
            for c in curNode.child:
                stack.append(curNode.child[c])
            tube = self.get_dryvr_tube(curNode)
            print("curNode: ", self.abs_reach[self.dryvrmode_to_virtualmode[curNode.nodeId]])
            print("curNode tube: ", tube)
            self.abs_reach[self.dryvrmode_to_virtualmode[curNode.nodeId]] = PolyUtils.merge_tubes([tube,
                                                                                                   self.abs_reach[self.dryvrmode_to_virtualmode[curNode.nodeId]]])
            print("the tube of parsed mode ", curNode.nodeId, ", denoted by ",
                  self.dryvrmode_to_virtualmode[curNode.nodeId], "is: ", self.abs_reach[self.dryvrmode_to_virtualmode[curNode.nodeId]])




    @staticmethod
    def get_agent_list_from_info(agents_info_list, snowflake_path = False):
        agents_list = []
        # trans_guards_union = pc.Region(list_poly=[])
        # trans_info_list = []
        # abs_graph_nodes = []
        if snowflake_path:
            P = VerificationScenario.generate_snowflake_waypoints(80, 2)
            rows_to_be_deleted = []
            for i in range(P.shape[0] - 1):
                if np.count_nonzero(P[i, :]) == 0 and np.count_nonzero(P[i + 1, :]) == 0:
                    rows_to_be_deleted.append(i)
            P = np.delete(P, rows_to_be_deleted, 0)
            P = np.hstack(
                (P, np.zeros((P.shape[0], 1))))  # HUSSEIN: UNCOMMENT IF 3D LINEAR EXAMPLE, COMMENT IF FIXEDWING
        abs_edges_guards: Dict[Tuple[Tuple, ...], List[Tuple[np.array, Tuple[float, ...], Tuple[float, ...]]]] = {}
        abs_edges_guards_union = {}
        # abs_trans_info: Dict[Tuple[List[List[float]]], List[List[Tuple[float, ...]]]]
        abs_reach: Dict[Tuple[float, ...], List[np.array]] = {}
        abs_initset: Dict[Tuple[List[float]], pc.Region] = {}
        abs_node_neighbors: Dict[Tuple[List[float]], List[Tuple[List[float]]]] = {}
        abs_agent_initset = []
        abs_timebound = {}
        for agent_info in agents_info_list:
            waypoint_list = []
            # agent_trans_info_list = []
            guard_list = agent_info["guards"]
            for ind, mode in enumerate(agent_info["mode_list"]):
                try:
                    if snowflake_path:
                        waypoint_list.append(
                            Waypoint("follow_waypoint", [P[ind, 0], P[ind, 1], P[ind, 2]],
                                     [[P[ind, 0] - 1, P[ind, 1] - 1, P[ind, 2] - 100], [P[ind, 0]
                                                                                        + 1,
                                                                                        P[ind,
                                                                                          1] + 1,
                                                                                        P[ind,
                                                                                          2] + 100]],
                                     20))
                    else:
                        waypoint_list.append(
                            Waypoint(mode[0], mode[1], guard_list[ind][1], agent_info["timeHorizons"][ind]))

                except IndexError:
                    pdb.set_trace()

            agents_list.append(Agent(waypoint_list, pc.box2poly(np.array(agent_info["initialSet"][1]).T),
                                     agent_info["directory"], importFunctions(agent_info["directory"])))
            for i in range(len(waypoint_list)):
                waypoint = waypoint_list[i]
                if i == 0:
                    prev_waypoint = Waypoint("follow_waypoint",
                                             np.average(PolyUtils.get_region_bounding_box(agents_list[-1].initial_set),
                                                        axis=0),
                                             PolyUtils.get_region_bounding_box(agents_list[-1].initial_set), 0)
                    transform_information1 = agents_list[-1].get_transform_information(waypoint.mode_parameters,
                                                                                       prev_waypoint.mode_parameters)
                    abs_agent_initset.append(PolyUtils.get_bounding_box(agents_list[-1].transform_poly_to_virtual(
                        agents_list[-1].initial_set, transform_information1)))

                   # print("initial set guard: ", PolyUtils.get_region_bounding_box(agents_list[-1].initial_set))
                else:
                    prev_waypoint = waypoint_list[i - 1]

                transform_information1 = agents_list[-1].get_transform_information(waypoint.mode_parameters,
                                                                                   prev_waypoint.mode_parameters)

                abs_prev_waypoint = agents_list[-1].transform_mode_to_virtual(prev_waypoint.mode_parameters,
                                                                              transform_information1)


                abs_node = tuple(abs_prev_waypoint)
                if i < len(waypoint_list) - 1:
                    next_waypoint = waypoint_list[i + 1]
                    transform_information2 = agents_list[-1].get_transform_information(next_waypoint.mode_parameters,
                                                                                       waypoint.mode_parameters)
                    abs_waypoint = agents_list[-1].transform_mode_to_virtual(waypoint.mode_parameters,
                                                                             transform_information2)
                    abs_edge = tuple([abs_node, tuple(abs_waypoint)])
                    if abs_node in abs_node_neighbors:
                        abs_node_neighbors[abs_node].append(tuple(abs_waypoint))
                    else:
                        abs_node_neighbors[abs_node] = [tuple(abs_waypoint)]
                    edge_guard_poly = agents_list[-1].transform_poly_to_virtual(
                        pc.box2poly(waypoint.original_guard.T),
                        transform_information1)
                    if abs_edge in abs_edges_guards:
                        abs_edges_guards_union[abs_edge] = pc.union(abs_edges_guards_union[abs_edge], edge_guard_poly)
                        abs_edges_guards[abs_edge].append(
                            (PolyUtils.get_bounding_box(edge_guard_poly),
                             transform_information1,
                             transform_information2))
                        abs_timebound[abs_edge] = max(abs_timebound[abs_edge], waypoint.time_bound)
                    else:
                        abs_edges_guards[abs_edge] = [(PolyUtils.get_bounding_box(edge_guard_poly), transform_information1,
                                                       transform_information2)]
                        abs_edges_guards_union[abs_edge] = pc.Region(list_poly=[edge_guard_poly])
                        abs_timebound[abs_edge] = waypoint.time_bound
                else:
                    if not abs_node in abs_node_neighbors:
                        abs_node_neighbors[abs_node] = []

                if not abs_node in abs_reach:
                    abs_reach[abs_node] = [] #  pc.Region(list_poly=[])

                if not abs_node in abs_initset:
                    abs_initset[abs_node] = pc.Region(list_poly=[])

                """    
                if abs_edge in abs_trans_info:
                    abs_trans_info[abs_edge].append([transform_information1, transform_information2])
                else:
                    abs_trans_info[abs_edge] = [[transform_information1, transform_information2]]
                abs_graph_nodes.append()
                agent_trans_info_list.append(transform_information)
                trans_guards_union = pc.union(trans_guards_union,
                                              )
                """
            # trans_info_list.append(agent_trans_info_list)

        # print("abs_edges_guards: ", abs_edges_guards)

        return agents_list, abs_agent_initset, abs_timebound, abs_edges_guards, abs_edges_guards_union, abs_reach, abs_initset, abs_node_neighbors  # trans_guards_union, trans_info_list

    @staticmethod
    def generate_snowflake_waypoints(side_len=1, n=1):
        P = np.array([np.array([0, 0]), np.array([side_len, 0]),
                      np.array([side_len * math.cos(-math.pi / 3), side_len * math.sin(-math.pi / 3)]),
                      np.array([0, 0])])
        for i in range(n):
            newP = np.zeros((P.shape[0] * 4 + 1, 2))
            for j in range(P.shape[0] - 1):
                newP[4 * j + 1, :] = P[j, :]
                # print("newp: ", newP[4 * j + 1, :], "P: ", P[j, :])
                newP[4 * j + 2, :] = (2 * P[j, :] + P[j + 1, :]) / 3
                link = P[j + 1, :] - P[j, :]
                ang = math.atan2(link[1], link[0])
                linkLeng = math.sqrt(link[0] * link[0] + link[1] * link[1])
                newP[4 * j + 3, :] = newP[4 * j + 2, :] + (linkLeng / 3) * np.array(
                    [math.cos(ang + math.pi / 3), math.sin(ang + math.pi / 3)])
                newP[4 * j + 4, :] = (P[j, :] + 2 * P[j + 1, :]) / 3

            newP[4 * P.shape[0], :] = P[P.shape[0] - 1, :]
            P = newP

        return P

    @staticmethod
    def get_agent_list_from_info_snowflake(agents_info_list):
        agents_list = []
        abs_edges_guards: Dict[Tuple[Tuple, ...], List[Tuple[np.array, Tuple[float, ...], Tuple[float, ...]]]] = {}
        abs_reach: Dict[Tuple[float, ...], pc.Region] = {}
        abs_initset: Dict[Tuple[List[float]], pc.Region] = {}
        abs_node_neighbors: Dict[Tuple[List[float]], List[Tuple[List[float]]]] = {}
        P = VerificationScenario.generate_snowflake_waypoints(80, 2)
        rows_to_be_deleted = []
        for i in range(P.shape[0]-1):
            if np.count_nonzero(P[i, :]) == 0 and np.count_nonzero(P[i+1, :]) == 0:
                rows_to_be_deleted.append(i)
        P = np.delete(P, rows_to_be_deleted, 0)
        P = np.hstack((P, np.zeros((P.shape[0], 1)))) # HUSSEIN: UNCOMMENT IF 3D LINEAR EXAMPLE, COMMENT IF FIXEDWING
        abs_agent_initset = []
        abs_timebound = {}
        for agent_info in agents_info_list:
            waypoint_list = []
            for ind in range(int(P.shape[0]/3)):
                try:
                    # HUSSEIN: UNCOMMENT IF 3D LINEAR EXAMPLE, COMMENT IF FIXEDWING

                    waypoint_list.append(
                        Waypoint("follow_waypoint", [P[ind, 0], P[ind, 1], P[ind, 2]], [[P[ind, 0] - 1, P[ind, 1] - 1, P[ind, 2] - 100], [P[ind, 0]
                                                                                                                + 1,
                                                                                                                P[ind,
                                                                                                                    1] + 1,
                                                                                                                P[ind,
                                                                                                                    2] + 100]],
                                 20))
                    """
                    waypoint_list.append(
                        Waypoint("follow_waypoint", [P[ind, 0], P[ind, 1]],
                                 [[P[ind, 0] - 4, P[ind, 1] - 4, -5, -1000], [P[ind, 0] + 4, P[ind, 1] + 4, 5, 1000]], 15))
                    """
                except IndexError:
                    pdb.set_trace()

            agents_list.append(Agent(waypoint_list, pc.box2poly(np.array(agent_info["initialSet"][1]).T),
                                     agent_info["directory"], importFunctions(agent_info["directory"])))
            for i in range(len(waypoint_list) - 1):
                waypoint = waypoint_list[i]
                if i == 0:
                    prev_waypoint = Waypoint("follow_waypoint",
                                             np.average(PolyUtils.get_region_bounding_box(agents_list[-1].initial_set),
                                                        axis=0),
                                             PolyUtils.get_region_bounding_box(agents_list[-1].initial_set), 0)
                    transform_information1 = agents_list[-1].get_transform_information(waypoint.mode_parameters,
                                                                                       prev_waypoint.mode_parameters)
                    abs_agent_initset.append(agents_list[-1].transform_poly_to_virtual(
                        agents_list[-1].initial_set, transform_information1))

                    # print("initial set guard: ", PolyUtils.get_region_bounding_box(agents_list[-1].initial_set))
                else:
                    prev_waypoint = waypoint_list[i - 1]
                next_waypoint = waypoint_list[i + 1]
                transform_information1 = agents_list[-1].get_transform_information(waypoint.mode_parameters,
                                                                                   prev_waypoint.mode_parameters)
                transform_information2 = agents_list[-1].get_transform_information(next_waypoint.mode_parameters,
                                                                                   waypoint.mode_parameters)
                abs_prev_waypoint = agents_list[-1].transform_mode_to_virtual(prev_waypoint.mode_parameters,
                                                                              transform_information1)
                abs_waypoint = agents_list[-1].transform_mode_to_virtual(waypoint.mode_parameters,
                                                                         transform_information2)
                abs_node = tuple(abs_prev_waypoint)
                abs_edge = tuple([abs_node, tuple(abs_waypoint)])
                if abs_node in abs_node_neighbors:
                    abs_node_neighbors[abs_node].append(tuple(abs_waypoint))
                else:
                    abs_node_neighbors[abs_node] = [tuple(abs_waypoint)]
                if abs_edge in abs_edges_guards:
                    abs_edges_guards[abs_edge].append(
                        (PolyUtils.get_bounding_box(agents_list[-1].transform_poly_to_virtual(
                            pc.box2poly(waypoint.original_guard.T),
                            transform_information1)),
                         transform_information1,
                         transform_information2))
                    abs_timebound[abs_edge] = max(abs_timebound[abs_edge], waypoint.time_bound)
                else:
                    abs_edges_guards[abs_edge] = [(PolyUtils.get_bounding_box(agents_list[-1].transform_poly_to_virtual(
                        pc.box2poly(waypoint.original_guard.T), transform_information1)), transform_information1,
                                                   transform_information2)]
                    abs_timebound[abs_edge] = waypoint.time_bound


                if not abs_node in abs_reach:
                    abs_reach[abs_node] = pc.Region(list_poly=[])

                if not abs_node in abs_initset:
                    abs_initset[abs_node] = pc.Region(list_poly=[])

                """    
                if abs_edge in abs_trans_info:
                    abs_trans_info[abs_edge].append([transform_information1, transform_information2])
                else:
                    abs_trans_info[abs_edge] = [[transform_information1, transform_information2]]
                abs_graph_nodes.append()
                agent_trans_info_list.append(transform_information)
                trans_guards_union = pc.union(trans_guards_union,
                                              )
                """
            # trans_info_list.append(agent_trans_info_list)

        return agents_list, abs_agent_initset, abs_timebound, abs_edges_guards, abs_reach, abs_initset, abs_node_neighbors

    @staticmethod
    def is_tube_static_safe(tubesegment: ReachtubeSegment, unsafeset: List[pc.Polytope] = []) -> bool:
        if len(unsafeset) <= 0:
            print("unsafeset empty")
            return True
        poly: pc.Polytope
        for poly in tubesegment.tube:
            # TODO: Maybe don't project down before intersecting...
            # we need the verify input format for this, but basically,
            # we should let the user leave dimensions that don't matter
            # as unbounded dimensions in the unsafe set.
            # PolyUtils.print_region(region)
            # unsafeset = PolyUtils.project_to_intersect(unsafeset, region)
            # print("unsafeset:", unsafeset)
            if all(PolyUtils.is_polytope_intersection_empty(unsafe_poly, poly) for unsafe_poly in unsafeset):
                return True
        return False

    def compute_agent_full_tube(self, cur_agent: Agent) -> Tuple[List[ReachtubeSegment], List[List[int]]]:
        cur_initset: pc.Region = cur_agent.initial_set
        full_tube: List[ReachtubeSegment] = []
        look_back: List[List[int]] = [[0, 0]]
        # list of pairs of index of a jump, look back number of indices
        # all polytopes in the tube between two consecutive indices would have a lookback equal to the previous one
        time_passed = 0
        last_waypoint = False
        # TODO update this loop to use the transform class
        for i in range(len(cur_agent.path)):
            waypoint: Waypoint = cur_agent.path[i]
            prev_waypoint: Waypoint
            if i == 0:
                prev_waypoint = Waypoint("follow_waypoint",
                                         np.average(PolyUtils.get_region_bounding_box(cur_initset), axis=0),
                                         PolyUtils.get_region_bounding_box(cur_initset), 0)
            else:
                prev_waypoint = cur_agent.path[i - 1]
            cur_tubesegment: ReachtubeSegment
            if i == len(cur_agent.path)-1:
                last_waypoint = True
                next_waypoint = [0] * cur_initset.dim
            else:
                next_waypoint: Waypoint = cur_agent.path[i + 1]
            cur_tubesegment, transform_time, fixed_point = self.tube_master.get_tubesegment(cur_agent, cur_initset,
                                                                                            waypoint,
                                                                                            prev_waypoint,
                                                                                            next_waypoint,
                                                                                            last_waypoint,
                                                                                            self.time_step)

            # model = self.agent_dynamics_ids[0]
            # we are assuming that all agents have the same model for now but we have to change later
            # print("self.trans_guards_union: ", self.trans_guards_union)
            # print("self.tube_master.tube_tools[model].tube_union: ", self.tube_master.tube_tools[model].tube_union)
            # print("self.tube_master.tube_tools[model].initset_union: ",
            #      self.tube_master.tube_tools[model].initset_union)
            # if pc.is_subset(all_possible_final_states_virtual,
            #                self.tube_master.tube_tools[model].initset_union):
            #    print("NO MORE CACHE MISSES, YEY! NO MORE CACHE MISSES, YEY! NO MORE CACHE MISSES, "
            #          "YEY! NO MORE CACHE MISSES, YEY!")

            # print("Done checking")

            cur_agent.transform_time += transform_time
            print("Segment ", i, " tube is computed with waypoint: ", waypoint.mode_parameters)
            # self.f.write("Segment " +  i, " tube is computed with waypoint: ", waypoint.mode_parameters)
            # TODO comment what these Asserts mean
            assert np.all(cur_tubesegment.system_origin == 0)
            assert cur_tubesegment.system_angle == 0
            assert cur_tubesegment.is_unified == 0
            look_back.append([look_back[-1][0] + len(cur_tubesegment.tube), cur_tubesegment.guard_max_index
                              - cur_tubesegment.guard_min_index])
            full_tube.append(cur_tubesegment)
            cur_initset: pc.Polytope
            # TAC, uncomment after
            if not fixed_point:
                cur_initset: pc.Polytope = pc.intersect(cur_tubesegment.next_initset,
                                                        pc.box2poly(waypoint.original_guard.T))
            time_passed: float = time_passed + cur_tubesegment.guard_min_index * self.time_step
        return full_tube, look_back


    def verify(self, dynamic_safety=True, use_dryvr=False):
        # TODO make lookback a numpy array instead
        if use_dryvr:
            dryvr_fixed_point = False
            dryvr_time_bound = 4
            # self.max_dryvr_time_horizon = 8
            while dryvr_time_bound <= self.max_dryvr_time_horizon and not dryvr_fixed_point:
                self.dryvr_path = "/Users/husseinsibai/Desktop/DryVR_0.2-master/"  # TODO: change to be an input
                self.dryvrmode_to_virtualmode = {}
                self.construct_dryvr_input_file(dryvr_time_bound)
                self.run_dryvr()
                self.parse_dryvr_output()
                dryvr_fixed_point = True
                # check for fixed point
                for abs_prev_waypoint in self.abs_reach:
                    for abs_cur_waypoint in self.abs_reach:
                        abs_cur_edge = tuple([abs_prev_waypoint, abs_cur_waypoint])
                        print("abs_cur_edge: ", abs_cur_edge)
                        if abs_cur_edge in self.abs_edges_guards:
                            print("it is an edge")
                            _, _, guards_inter_list = TubeMaster.intersect_waypoint_list(
                                self.abs_reach[abs_prev_waypoint],
                                self.abs_edges_guards[abs_cur_edge])
                            print("guards_inter_list: ", guards_inter_list)
                            for rect in guards_inter_list:
                                possible_next_initset_rect = PolyUtils.get_bounding_box(
                                    self.agents_list[-1].transform_poly_to_virtual(
                                        self.agents_list[-1].transform_poly_from_virtual(
                                            pc.box2poly(rect[0].T),
                                            rect[1]), rect[2]))
                                print("abs_cur_waypoint: ", abs_cur_waypoint)
                                print("possible_next_initset_rect:", possible_next_initset_rect)
                                if len(self.abs_reach[abs_cur_waypoint]) > 0:
                                    print("self.abs_reach[abs_cur_waypoint][0]:", self.abs_reach[abs_cur_waypoint][0])
                                    if not PolyUtils.does_rect_contain(possible_next_initset_rect, self.abs_reach[abs_cur_waypoint][0]):
                                        print("INITIAL SET NOT COMPUTED YET :/")
                                        dryvr_fixed_point = False
                                        break
                                else:
                                    dryvr_fixed_point = False
                                    break

                            if not dryvr_fixed_point:
                                break
                        if not dryvr_fixed_point:
                            break
                    if not dryvr_fixed_point:
                        break
                if dryvr_fixed_point:
                    break
                else:
                    dryvr_time_bound = dryvr_time_bound * 2
                print("dryvr_time_bound: ", dryvr_time_bound)

            if dryvr_fixed_point:
                print("FIXED POINT HAS BEEN REACHED")
            self.tube_master.reached_fixed_point = True
        t = time.time()
        agents_look_back: List[List[List[int]]] = []
        max_tube_length = 0
        prev_computed = 0
        prev_saved = 0
        prev_ultrasaved = 0
        for i in range(self.num_agents):
            agent = self.agents_list[i]
            # only checks static safety
            agent_tubesegments: List[ReachtubeSegment]
            agent_look_back: List[List[int]]
            agent_tubesegments, agent_look_back = self.compute_agent_full_tube(agent)
            print("initset volume: ")
            volume_list = []
            for agent_tubesegment in agent_tubesegments:
                volume_list.append(PolyUtils.get_rect_volume(PolyUtils.get_bounding_box(agent_tubesegment.tube[0])))
                #print("Is segment safe?", self.is_tube_static_safe(agent_tubesegment, self.unsafe_set))
            print(volume_list)
            if agent_look_back[-1][0] > max_tube_length:
                max_tube_length = agent_look_back[-1][0]
            self.agents_full_tubes.append(agent_tubesegments)
            agents_look_back.append(agent_look_back)
            print("finished agent #", len(self.agents_full_tubes))
            model = self.agent_dynamics_ids[i]
            print("Number of computed tubes: ", self.tube_master.tube_tools[model].computed_counter - prev_computed)
            print("Number of saved tubes: ", self.tube_master.tube_tools[model].saved_counter - prev_saved)
            print("Number of saved without search tubes: ", self.tube_master.tube_tools[model].ultrasaved_counter - prev_ultrasaved)
            print("Number of virtual modes: ", len(self.abs_initset))
            print("Number of virtual edges: ", len(self.abs_edges_guards))
            prev_computed = self.tube_master.tube_tools[model].computed_counter
            prev_saved = self.tube_master.tube_tools[model].saved_counter
            prev_ultrasaved = self.tube_master.tube_tools[model].ultrasaved_counter

        # pdb.set_trace()
        if dynamic_safety:
            agents_segment_idx = [0] * self.num_agents
            agents_segment_entry_idx = [0] * self.num_agents
            for i in range(max_tube_length):
                for d1 in range(self.num_agents):  # this would not be true once we have agents of different types
                    # if i > agents_look_back[d1][-1][0]:
                    #   continue
                    if agents_segment_idx[d1] == -1:
                        continue
                    # if i > agents_look_back[d1][agents_segment_idx[d1]][0]:
                    #    agents_segment_idx[d1] += 1
                    #    agents_segment_entry_idx[d1] = 0
                    d1_cur_look_back = agents_look_back[d1][agents_segment_idx[d1]][1]
                    for d2 in range(d1):
                        min_d2_rel_idx = i - d1_cur_look_back
                        if i - d1_cur_look_back >= agents_look_back[d2][agents_segment_idx[d2]][0]:
                            continue
                        d2_cur_segment_idx = agents_segment_idx[d2]
                        d2_cur_segment_entry_idx = agents_segment_entry_idx[d2]
                        if d2_cur_segment_idx == -1:
                            j = i
                            while j >= agents_look_back[d2][-1][0]:
                                d1_cur_look_back -= 1
                                j -= 1
                            d2_cur_segment_idx = len(self.agents_full_tubes[d2]) - 1
                            d2_cur_segment_entry_idx = len(self.agents_full_tubes[d2][d2_cur_segment_idx].tube) - 1

                        d2_num_steps_back = d1_cur_look_back

                        #    d1_cur_look_back -= 1
                        # if j >= agents_look_back[d2][agents_segment_idx[d2]][0] and agents_segment_idx[d2] < len(self.agents_full_tubes[d2]) - 1:
                        #    agents_segment_idx[d2] += 1
                        # else:
                        #    j = i
                        # maximization with zero here is just for safety but otherwise it shouldn't be needed
                        while d2_num_steps_back > 0:
                            if d2_cur_segment_entry_idx < d2_num_steps_back:
                                if d2_cur_segment_idx > 0:
                                    d2_num_steps_back -= d2_cur_segment_entry_idx
                                    d2_cur_segment_idx -= 1
                                    d2_cur_segment_entry_idx = len(self.agents_full_tubes[d2][d2_cur_segment_idx].tube) \
                                                               - 1
                                else:
                                    d2_num_steps_back = 0
                                    d2_cur_segment_entry_idx = 0
                                    d2_cur_segment_idx = 0
                            else:
                                d2_cur_segment_entry_idx -= d2_num_steps_back
                                d2_num_steps_back = 0
                        counter = 0
                        d2_look_back_includes_i = False
                        # d2_cur_segment_entry_idx + \
                        #                             agents_look_back[d2][d2_cur_segment_idx][0] - \
                        #                             agents_look_back[d2][d2_cur_segment_idx][1] <= i
                        while counter < d1_cur_look_back or d2_look_back_includes_i:
                            # check the intersection of full_tube[d1][i] and full_tube[d2][j]
                            # if not pc.is_empty(pc.intersect(
                            if not PolyUtils.is_polytope_intersection_empty(
                                    self.agents_full_tubes[d1][agents_segment_idx[d1]].tube[
                                        agents_segment_entry_idx[d1]],
                                    self.agents_full_tubes[d2][d2_cur_segment_idx].tube[d2_cur_segment_entry_idx]):
                                print("The system is unsafe since drone ", d1, " intersects with drone ", d2,
                                      "at time ", i
                                      * self.time_step)
                                # return False
                            d2_cur_segment_entry_idx += 1
                            if d2_cur_segment_entry_idx >= len(self.agents_full_tubes[d2][d2_cur_segment_idx].tube):
                                d2_cur_segment_idx += 1
                                if d2_cur_segment_idx >= len(self.agents_full_tubes[d2]):
                                    break
                                d2_cur_segment_entry_idx = 0
                            d2_look_back_includes_i = d2_cur_segment_entry_idx + \
                                                      agents_look_back[d2][d2_cur_segment_idx][0] \
                                                      - agents_look_back[d2][d2_cur_segment_idx][1] <= i
                            counter += 1
                    for d in range(self.num_agents):
                        if agents_segment_idx[d] > -1:
                            agents_segment_entry_idx[d] += 1
                            if agents_segment_entry_idx[d] >= len(
                                    self.agents_full_tubes[d][agents_segment_idx[d]].tube):
                                agents_segment_idx[d] += 1
                                agents_segment_entry_idx[d] = 0
                            if agents_segment_idx[d] >= len(self.agents_full_tubes[d]):
                                agents_segment_idx[d] = -1

        # print("agents look back: ", agents_look_back)
        # print("max_tube_length: ", max_tube_length)
        for model in set(self.agent_dynamics_ids):
            print("Final Number of computed tubes: ", self.tube_master.tube_tools[model].computed_counter)
            print("Final Number of saved tubes: ", self.tube_master.tube_tools[model].saved_counter)
            print("Final Number of ultrasaved tubes: ", self.tube_master.tube_tools[model].ultrasaved_counter)

        print("execution time: ", (time.time() - t) / 60.0)
        print("transform time per agent:", [agent.transform_time / 60.0 for agent in self.agents_list])
        return True

    def plot(self):
        is_unified = False
        if self.tube_master.sym_level != 0:
            abs_reach = self.abs_reach # self.tube_master.tube_tools[self.agents_list[-1].dynamics].abs_reach
            virtual_mode_to_id = {}
            virtual_mode_counter = 0
            for virtual_mode in abs_reach:
                if not virtual_mode in virtual_mode_to_id:
                    virtual_mode_to_id[virtual_mode] = virtual_mode_counter
                    virtual_mode_counter += 1
            Plotter.plot(self.num_agents, self.agents_full_tubes, self.agents_list, is_unified, self.unsafe_set, virtual_mode_to_id,
                         plot_step=1)
            Plotter.plot_virtual(abs_reach, virtual_mode_to_id, self.abs_nodes_neighbors, plot_step=1)
        else:
            Plotter.plot(self.num_agents, self.agents_full_tubes, self.agents_list, is_unified, self.unsafe_set,
                         None,
                         plot_step=1)
        pass


def main():
    # scene = VerificationScenario(str(os.path.abspath(
    #    './../examples/scenarios/Linear3D_S_trans_2D.json')), "Linear3D_S_trans_2D.json")  # Linear3D_S_trans_2D # Drone3D_S.json # ThreeFixwingedDrones_3d # snowflake.json
    # result = scene.verify(False)
    scene = VerificationScenario(str(os.path.abspath(sys.argv[1])))
    result = scene.verify(use_dryvr=False)
    scene.plot()
    # TODO support return of unsafe result.
    if type(result) == bool and result:
        print("System is safe")
    else:
        print("System safety is unknown")


if __name__ == '__main__':
    # main()
    # pr = cProfile.Profile()
    # pr.enable()
    main()
    # pr.disable()
    # pr.print_stats()
