WARNING: Not monitoring node memory since `psutil` is not installed. Install this with `pip install psutil` (or ray[debug]) to enable debugging of memory-related crashes.
warning, called region bbox function on polytope
warning, called region bbox function on polytope
abs_edges_guards:  {((-5.0, -5.0, 0.0), (-9.0, 0.0, 0.0)): [(array([[  -1.,   -1., -100.],
       [   1.,    1.,  100.]]), (array([-0., -0., -0.]), 0.7853981633974483), (array([-8.88889, -0.     , -0.     ]), 1.5707963267948966))], ((-9.0, 0.0, 0.0), (-4.0, -8.0, 0.0)): [(array([[  -1.,   -1., -100.],
       [   1.,    1.,  100.]]), (array([-8.88889, -0.     , -0.     ]), 1.5707963267948966), (array([-13.33333,  -7.698  ,  -0.     ]), 0.523598775598299)), (array([[  -1.,   -1., -100.],
       [   1.,    1.,  100.]]), (array([-26.66667,  -0.     ,  -0.     ]), 1.5707963267948966), (array([-31.11111,  -7.698  ,  -0.     ]), 0.523598775598299)), (array([[  -1.,   -1., -100.],
       [   1.,    1.,  100.]]), (array([-35.55556, -15.39601,  -0.     ]), 1.5707963267948968), (array([-40.     , -23.09401,  -0.     ]), 0.5235987755982988)), (array([[  -1.,   -1., -100.],
       [   1.,    1.,  100.]]), (array([-62.22222,  -0.     ,  -0.     ]), 1.5707963267948966), (array([-66.66667,  -7.698  ,  -0.     ]), 0.5235987755982988))], ((-4.0, -8.0, 0.0), (-4.0, 8.0, 0.0)): [(array([[  -1.,   -1., -100.],
       [   1.,    1.,  100.]]), (array([-13.33333,  -7.698  ,  -0.     ]), 0.523598775598299), (array([-17.77778,  -0.     ,  -0.     ]), 2.6179938779914944)), (array([[  -1.,   -1., -100.],
       [   1.,    1.,  100.]]), (array([-40.     , -23.09401,  -0.     ]), 0.5235987755982988), (array([-44.44444, -15.39601,  -0.     ]), 2.6179938779914944)), (array([[  -1.,   -1., -100.],
       [   1.,    1.,  100.]]), (array([-66.66667,  -7.698  ,  -0.     ]), 0.5235987755982988), (array([-71.11111,  -0.     ,  -0.     ]), 2.6179938779914944))], ((-4.0, 8.0, 0.0), (-9.0, 0.0, 0.0)): [(array([[  -1.,   -1., -100.],
       [   1.,    1.,  100.]]), (array([-17.77778,  -0.     ,  -0.     ]), 2.6179938779914944), (array([-26.66667,  -0.     ,  -0.     ]), 1.5707963267948966)), (array([[  -1.,   -1., -100.],
       [   1.,    1.,  100.]]), (array([-44.44444, -15.39601,  -0.     ]), 2.6179938779914944), (array([-53.33333, -15.39601,  -0.     ]), 1.5707963267948968)), (array([[  -1.,   -1., -100.],
       [   1.,    1.,  100.]]), (array([-53.33333,  -0.     ,  -0.     ]), 2.6179938779914944), (array([-62.22222,  -0.     ,  -0.     ]), 1.5707963267948966))], ((-4.0, -8.0, 0.0), (4.0, -8.0, 0.0)): [(array([[  -1.,   -1., -100.],
       [   1.,    1.,  100.]]), (array([-31.11111,  -7.698  ,  -0.     ]), 0.523598775598299), (array([-26.66667, -15.39601,  -0.     ]), -0.5235987755982986))], ((4.0, -8.0, 0.0), (-9.0, 0.0, 0.0)): [(array([[  -1.,   -1., -100.],
       [   1.,    1.,  100.]]), (array([-26.66667, -15.39601,  -0.     ]), -0.5235987755982986), (array([-35.55556, -15.39601,  -0.     ]), 1.5707963267948968))], ((-9.0, 0.0, 0.0), (4.0, 8.0, 0.0)): [(array([[  -1.,   -1., -100.],
       [   1.,    1.,  100.]]), (array([-53.33333, -15.39601,  -0.     ]), 1.5707963267948968), (array([-48.88889,  -7.698  ,  -0.     ]), -2.6179938779914944))], ((4.0, 8.0, 0.0), (-4.0, 8.0, 0.0)): [(array([[  -1.,   -1., -100.],
       [   1.,    1.,  100.]]), (array([-48.88889,  -7.698  ,  -0.     ]), -2.6179938779914944), (array([-53.33333,  -0.     ,  -0.     ]), 2.6179938779914944))]}
Great, symmetry is used WITH fixed point checking
warning, called region bbox function on polytope
warning, called region bbox function on polytope
len(self.uncovered_sets.list_poly) total :  1
len(self.uncovered_sets.list_poly),  0 th  mode  (-5.0, -5.0, 0.0)  is:  0
len(self.uncovered_sets.list_poly),  1 th  mode  (-9.0, 0.0, 0.0)  is:  1
len(self.uncovered_sets.list_poly),  2 th  mode  (-4.0, -8.0, 0.0)  is:  0
len(self.uncovered_sets.list_poly),  3 th  mode  (-4.0, 8.0, 0.0)  is:  0
len(self.uncovered_sets.list_poly),  4 th  mode  (4.0, -8.0, 0.0)  is:  0
len(self.uncovered_sets.list_poly),  5 th  mode  (4.0, 8.0, 0.0)  is:  0
self.abs_initset:  {(-5.0, -5.0, 0.0): <polytope.polytope.Polytope object at 0x12c80c390>, (-9.0, 0.0, 0.0): <polytope.polytope.Region object at 0x12d4edad0>, (-4.0, -8.0, 0.0): <polytope.polytope.Region object at 0x12d4edc90>, (-4.0, 8.0, 0.0): <polytope.polytope.Region object at 0x12d4f24d0>, (4.0, -8.0, 0.0): <polytope.polytope.Region object at 0x12d4f25d0>, (4.0, 8.0, 0.0): <polytope.polytope.Region object at 0x12d4f2710>}
Segment  0  tube is computed with waypoint:  [0.0, 0.0, 0.0]
len(self.uncovered_sets.list_poly) total :  18
len(self.uncovered_sets.list_poly),  0 th  mode  (-5.0, -5.0, 0.0)  is:  0
len(self.uncovered_sets.list_poly),  1 th  mode  (-9.0, 0.0, 0.0)  is:  1
len(self.uncovered_sets.list_poly),  2 th  mode  (-4.0, -8.0, 0.0)  is:  16
len(self.uncovered_sets.list_poly),  3 th  mode  (-4.0, 8.0, 0.0)  is:  0
len(self.uncovered_sets.list_poly),  4 th  mode  (4.0, -8.0, 0.0)  is:  0
len(self.uncovered_sets.list_poly),  5 th  mode  (4.0, 8.0, 0.0)  is:  1
self.abs_initset:  {(-5.0, -5.0, 0.0): <polytope.polytope.Polytope object at 0x12c80c390>, (-9.0, 0.0, 0.0): <polytope.polytope.Polytope object at 0x123abbf90>, (-4.0, -8.0, 0.0): <polytope.polytope.Region object at 0x12d4edc90>, (-4.0, 8.0, 0.0): <polytope.polytope.Region object at 0x12d4f24d0>, (4.0, -8.0, 0.0): <polytope.polytope.Region object at 0x12d4f25d0>, (4.0, 8.0, 0.0): <polytope.polytope.Region object at 0x12d4f2710>}
Segment  1  tube is computed with waypoint:  [8.88888888888889, 0.0, 0.0]
len(self.uncovered_sets.list_poly) total :  27
len(self.uncovered_sets.list_poly),  0 th  mode  (-5.0, -5.0, 0.0)  is:  0
len(self.uncovered_sets.list_poly),  1 th  mode  (-9.0, 0.0, 0.0)  is:  0
len(self.uncovered_sets.list_poly),  2 th  mode  (-4.0, -8.0, 0.0)  is:  16
len(self.uncovered_sets.list_poly),  3 th  mode  (-4.0, 8.0, 0.0)  is:  9
len(self.uncovered_sets.list_poly),  4 th  mode  (4.0, -8.0, 0.0)  is:  1
len(self.uncovered_sets.list_poly),  5 th  mode  (4.0, 8.0, 0.0)  is:  1
self.abs_initset:  {(-5.0, -5.0, 0.0): <polytope.polytope.Polytope object at 0x12c80c390>, (-9.0, 0.0, 0.0): <polytope.polytope.Polytope object at 0x123abbf90>, (-4.0, -8.0, 0.0): <polytope.polytope.Polytope object at 0x123f63910>, (-4.0, 8.0, 0.0): <polytope.polytope.Region object at 0x12d4f24d0>, (4.0, -8.0, 0.0): <polytope.polytope.Region object at 0x12d4f25d0>, (4.0, 8.0, 0.0): <polytope.polytope.Region object at 0x12d4f2710>}
Segment  2  tube is computed with waypoint:  [13.333333333333336, 7.69800358919501, 0.0]
len(self.uncovered_sets.list_poly) total :  11
len(self.uncovered_sets.list_poly),  0 th  mode  (-5.0, -5.0, 0.0)  is:  0
len(self.uncovered_sets.list_poly),  1 th  mode  (-9.0, 0.0, 0.0)  is:  0
len(self.uncovered_sets.list_poly),  2 th  mode  (-4.0, -8.0, 0.0)  is:  0
len(self.uncovered_sets.list_poly),  3 th  mode  (-4.0, 8.0, 0.0)  is:  9
len(self.uncovered_sets.list_poly),  4 th  mode  (4.0, -8.0, 0.0)  is:  1
len(self.uncovered_sets.list_poly),  5 th  mode  (4.0, 8.0, 0.0)  is:  1
self.abs_initset:  {(-5.0, -5.0, 0.0): <polytope.polytope.Polytope object at 0x12c80c390>, (-9.0, 0.0, 0.0): <polytope.polytope.Polytope object at 0x123abbf90>, (-4.0, -8.0, 0.0): <polytope.polytope.Polytope object at 0x123f63910>, (-4.0, 8.0, 0.0): <polytope.polytope.Polytope object at 0x12d97a0d0>, (4.0, -8.0, 0.0): <polytope.polytope.Region object at 0x12d4f25d0>, (4.0, 8.0, 0.0): <polytope.polytope.Region object at 0x12d4f2710>}
Segment  3  tube is computed with waypoint:  [17.77777777777778, 0.0, 0.0]
len(self.uncovered_sets.list_poly) total :  3
len(self.uncovered_sets.list_poly),  0 th  mode  (-5.0, -5.0, 0.0)  is:  0
len(self.uncovered_sets.list_poly),  1 th  mode  (-9.0, 0.0, 0.0)  is:  0
len(self.uncovered_sets.list_poly),  2 th  mode  (-4.0, -8.0, 0.0)  is:  0
len(self.uncovered_sets.list_poly),  3 th  mode  (-4.0, 8.0, 0.0)  is:  0
len(self.uncovered_sets.list_poly),  4 th  mode  (4.0, -8.0, 0.0)  is:  1
len(self.uncovered_sets.list_poly),  5 th  mode  (4.0, 8.0, 0.0)  is:  2
self.abs_initset:  {(-5.0, -5.0, 0.0): <polytope.polytope.Polytope object at 0x12c80c390>, (-9.0, 0.0, 0.0): <polytope.polytope.Region object at 0x1242e8dd0>, (-4.0, -8.0, 0.0): <polytope.polytope.Polytope object at 0x123f63910>, (-4.0, 8.0, 0.0): <polytope.polytope.Polytope object at 0x12d97a0d0>, (4.0, -8.0, 0.0): <polytope.polytope.Region object at 0x12d4f25d0>, (4.0, 8.0, 0.0): <polytope.polytope.Region object at 0x12d4f2710>}
Segment  4  tube is computed with waypoint:  [26.666666666666668, 0.0, 0.0]
len(self.uncovered_sets.list_poly) total :  4
len(self.uncovered_sets.list_poly),  0 th  mode  (-5.0, -5.0, 0.0)  is:  0
len(self.uncovered_sets.list_poly),  1 th  mode  (-9.0, 0.0, 0.0)  is:  0
len(self.uncovered_sets.list_poly),  2 th  mode  (-4.0, -8.0, 0.0)  is:  0
len(self.uncovered_sets.list_poly),  3 th  mode  (-4.0, 8.0, 0.0)  is:  0
len(self.uncovered_sets.list_poly),  4 th  mode  (4.0, -8.0, 0.0)  is:  2
len(self.uncovered_sets.list_poly),  5 th  mode  (4.0, 8.0, 0.0)  is:  2
self.abs_initset:  {(-5.0, -5.0, 0.0): <polytope.polytope.Polytope object at 0x12c80c390>, (-9.0, 0.0, 0.0): <polytope.polytope.Region object at 0x1242e8dd0>, (-4.0, -8.0, 0.0): <polytope.polytope.Region object at 0x12da62910>, (-4.0, 8.0, 0.0): <polytope.polytope.Polytope object at 0x12d97a0d0>, (4.0, -8.0, 0.0): <polytope.polytope.Region object at 0x12d4f25d0>, (4.0, 8.0, 0.0): <polytope.polytope.Region object at 0x12d4f2710>}
Segment  5  tube is computed with waypoint:  [31.111111111111114, 7.69800358919501, 0.0]
len(self.uncovered_sets.list_poly) total :  4
len(self.uncovered_sets.list_poly),  0 th  mode  (-5.0, -5.0, 0.0)  is:  0
len(self.uncovered_sets.list_poly),  1 th  mode  (-9.0, 0.0, 0.0)  is:  0
len(self.uncovered_sets.list_poly),  2 th  mode  (-4.0, -8.0, 0.0)  is:  0
len(self.uncovered_sets.list_poly),  3 th  mode  (-4.0, 8.0, 0.0)  is:  0
len(self.uncovered_sets.list_poly),  4 th  mode  (4.0, -8.0, 0.0)  is:  2
len(self.uncovered_sets.list_poly),  5 th  mode  (4.0, 8.0, 0.0)  is:  2
self.abs_initset:  {(-5.0, -5.0, 0.0): <polytope.polytope.Polytope object at 0x12c80c390>, (-9.0, 0.0, 0.0): <polytope.polytope.Region object at 0x1242e8dd0>, (-4.0, -8.0, 0.0): <polytope.polytope.Region object at 0x12da62910>, (-4.0, 8.0, 0.0): <polytope.polytope.Polytope object at 0x12d97a0d0>, (4.0, -8.0, 0.0): <polytope.polytope.Polytope object at 0x12dc2ef10>, (4.0, 8.0, 0.0): <polytope.polytope.Region object at 0x12d4f2710>}
Segment  6  tube is computed with waypoint:  [26.66666666666667, 15.396007178390022, 0.0]
len(self.uncovered_sets.list_poly) total :  3
len(self.uncovered_sets.list_poly),  0 th  mode  (-5.0, -5.0, 0.0)  is:  0
len(self.uncovered_sets.list_poly),  1 th  mode  (-9.0, 0.0, 0.0)  is:  0
len(self.uncovered_sets.list_poly),  2 th  mode  (-4.0, -8.0, 0.0)  is:  0
len(self.uncovered_sets.list_poly),  3 th  mode  (-4.0, 8.0, 0.0)  is:  0
len(self.uncovered_sets.list_poly),  4 th  mode  (4.0, -8.0, 0.0)  is:  0
len(self.uncovered_sets.list_poly),  5 th  mode  (4.0, 8.0, 0.0)  is:  3
self.abs_initset:  {(-5.0, -5.0, 0.0): <polytope.polytope.Polytope object at 0x12c80c390>, (-9.0, 0.0, 0.0): <polytope.polytope.Region object at 0x12db15e10>, (-4.0, -8.0, 0.0): <polytope.polytope.Region object at 0x12da62910>, (-4.0, 8.0, 0.0): <polytope.polytope.Polytope object at 0x12d97a0d0>, (4.0, -8.0, 0.0): <polytope.polytope.Polytope object at 0x12dc2ef10>, (4.0, 8.0, 0.0): <polytope.polytope.Region object at 0x12d4f2710>}
Segment  7  tube is computed with waypoint:  [35.555555555555564, 15.39600717839002, 0.0]
len(self.uncovered_sets.list_poly) total :  3
len(self.uncovered_sets.list_poly),  0 th  mode  (-5.0, -5.0, 0.0)  is:  0
len(self.uncovered_sets.list_poly),  1 th  mode  (-9.0, 0.0, 0.0)  is:  0
len(self.uncovered_sets.list_poly),  2 th  mode  (-4.0, -8.0, 0.0)  is:  0
len(self.uncovered_sets.list_poly),  3 th  mode  (-4.0, 8.0, 0.0)  is:  0
len(self.uncovered_sets.list_poly),  4 th  mode  (4.0, -8.0, 0.0)  is:  0
len(self.uncovered_sets.list_poly),  5 th  mode  (4.0, 8.0, 0.0)  is:  3
self.abs_initset:  {(-5.0, -5.0, 0.0): <polytope.polytope.Polytope object at 0x12c80c390>, (-9.0, 0.0, 0.0): <polytope.polytope.Region object at 0x12db15e10>, (-4.0, -8.0, 0.0): <polytope.polytope.Region object at 0x12dd515d0>, (-4.0, 8.0, 0.0): <polytope.polytope.Polytope object at 0x12d97a0d0>, (4.0, -8.0, 0.0): <polytope.polytope.Polytope object at 0x12dc2ef10>, (4.0, 8.0, 0.0): <polytope.polytope.Region object at 0x12d4f2710>}
Segment  8  tube is computed with waypoint:  [40.00000000000001, 23.09401076758503, 0.0]
len(self.uncovered_sets.list_poly) total :  3
len(self.uncovered_sets.list_poly),  0 th  mode  (-5.0, -5.0, 0.0)  is:  0
len(self.uncovered_sets.list_poly),  1 th  mode  (-9.0, 0.0, 0.0)  is:  0
len(self.uncovered_sets.list_poly),  2 th  mode  (-4.0, -8.0, 0.0)  is:  0
len(self.uncovered_sets.list_poly),  3 th  mode  (-4.0, 8.0, 0.0)  is:  0
len(self.uncovered_sets.list_poly),  4 th  mode  (4.0, -8.0, 0.0)  is:  0
len(self.uncovered_sets.list_poly),  5 th  mode  (4.0, 8.0, 0.0)  is:  3
self.abs_initset:  {(-5.0, -5.0, 0.0): <polytope.polytope.Polytope object at 0x12c80c390>, (-9.0, 0.0, 0.0): <polytope.polytope.Region object at 0x12db15e10>, (-4.0, -8.0, 0.0): <polytope.polytope.Region object at 0x12dd515d0>, (-4.0, 8.0, 0.0): <polytope.polytope.Region object at 0x12dd7ce10>, (4.0, -8.0, 0.0): <polytope.polytope.Polytope object at 0x12dc2ef10>, (4.0, 8.0, 0.0): <polytope.polytope.Region object at 0x12d4f2710>}
Segment  9  tube is computed with waypoint:  [44.44444444444445, 15.39600717839002, 0.0]
len(self.uncovered_sets.list_poly) total :  4
len(self.uncovered_sets.list_poly),  0 th  mode  (-5.0, -5.0, 0.0)  is:  0
len(self.uncovered_sets.list_poly),  1 th  mode  (-9.0, 0.0, 0.0)  is:  0
len(self.uncovered_sets.list_poly),  2 th  mode  (-4.0, -8.0, 0.0)  is:  0
len(self.uncovered_sets.list_poly),  3 th  mode  (-4.0, 8.0, 0.0)  is:  0
len(self.uncovered_sets.list_poly),  4 th  mode  (4.0, -8.0, 0.0)  is:  0
len(self.uncovered_sets.list_poly),  5 th  mode  (4.0, 8.0, 0.0)  is:  4
self.abs_initset:  {(-5.0, -5.0, 0.0): <polytope.polytope.Polytope object at 0x12c80c390>, (-9.0, 0.0, 0.0): <polytope.polytope.Region object at 0x12dd7ce50>, (-4.0, -8.0, 0.0): <polytope.polytope.Region object at 0x12dd515d0>, (-4.0, 8.0, 0.0): <polytope.polytope.Region object at 0x12dd7ce10>, (4.0, -8.0, 0.0): <polytope.polytope.Polytope object at 0x12dc2ef10>, (4.0, 8.0, 0.0): <polytope.polytope.Region object at 0x12d4f2710>}
Segment  10  tube is computed with waypoint:  [53.333333333333336, 15.396007178390018, 0.0]
len(self.uncovered_sets.list_poly) total :  5
len(self.uncovered_sets.list_poly),  0 th  mode  (-5.0, -5.0, 0.0)  is:  0
len(self.uncovered_sets.list_poly),  1 th  mode  (-9.0, 0.0, 0.0)  is:  0
len(self.uncovered_sets.list_poly),  2 th  mode  (-4.0, -8.0, 0.0)  is:  0
len(self.uncovered_sets.list_poly),  3 th  mode  (-4.0, 8.0, 0.0)  is:  1
len(self.uncovered_sets.list_poly),  4 th  mode  (4.0, -8.0, 0.0)  is:  0
len(self.uncovered_sets.list_poly),  5 th  mode  (4.0, 8.0, 0.0)  is:  4
self.abs_initset:  {(-5.0, -5.0, 0.0): <polytope.polytope.Polytope object at 0x12c80c390>, (-9.0, 0.0, 0.0): <polytope.polytope.Region object at 0x12dd7ce50>, (-4.0, -8.0, 0.0): <polytope.polytope.Region object at 0x12dd515d0>, (-4.0, 8.0, 0.0): <polytope.polytope.Region object at 0x12dd7ce10>, (4.0, -8.0, 0.0): <polytope.polytope.Polytope object at 0x12dc2ef10>, (4.0, 8.0, 0.0): <polytope.polytope.Polytope object at 0x12db15e10>}
Segment  11  tube is computed with waypoint:  [48.88888888888889, 7.69800358919501, 0.0]
len(self.uncovered_sets.list_poly) total :  1
len(self.uncovered_sets.list_poly),  0 th  mode  (-5.0, -5.0, 0.0)  is:  0
len(self.uncovered_sets.list_poly),  1 th  mode  (-9.0, 0.0, 0.0)  is:  0
len(self.uncovered_sets.list_poly),  2 th  mode  (-4.0, -8.0, 0.0)  is:  0
len(self.uncovered_sets.list_poly),  3 th  mode  (-4.0, 8.0, 0.0)  is:  1
len(self.uncovered_sets.list_poly),  4 th  mode  (4.0, -8.0, 0.0)  is:  0
len(self.uncovered_sets.list_poly),  5 th  mode  (4.0, 8.0, 0.0)  is:  0
FIXED POINT HAS BEEN REACHED!!!
YEY! NO MORE CACHE MISSES
Segment  12  tube is computed with waypoint:  [53.333333333333336, 0.0, 0.0]
Segment  13  tube is computed with waypoint:  [62.22222222222223, 0.0, 0.0]
Segment  14  tube is computed with waypoint:  [66.66666666666667, 7.698003589195008, 0.0]
Segment  15  tube is computed with waypoint:  [71.11111111111111, 0.0, 0.0]
Is segment safe? True
Is segment safe? True
Is segment safe? True
Is segment safe? True
Is segment safe? True
Is segment safe? True
Is segment safe? True
Is segment safe? True
Is segment safe? True
Is segment safe? True
Is segment safe? True
Is segment safe? True
Is segment safe? True
Is segment safe? True
Is segment safe? True
Is segment safe? True
finished agent # 1
Number of computed tubes:  496
Number of saved tubes:  252
Number of saved without search tubes:  271
Final Number of computed tubes:  496
Final Number of saved tubes:  252
Final Number of ultrasaved tubes:  271
execution time:  1.4099268317222595
transform time per agent: [0.020208088556925456]
(-5.0, -5.0, 0.0) -> (-9.0, 0.0, 0.0)
(-9.0, 0.0, 0.0) -> (-4.0, -8.0, 0.0)
(-9.0, 0.0, 0.0) -> (-4.0, -8.0, 0.0)
(-9.0, 0.0, 0.0) -> (-4.0, -8.0, 0.0)
(-9.0, 0.0, 0.0) -> (4.0, 8.0, 0.0)
(-9.0, 0.0, 0.0) -> (-4.0, -8.0, 0.0)
(-4.0, -8.0, 0.0) -> (-4.0, 8.0, 0.0)
(-4.0, -8.0, 0.0) -> (4.0, -8.0, 0.0)
(-4.0, -8.0, 0.0) -> (-4.0, 8.0, 0.0)
(-4.0, -8.0, 0.0) -> (-4.0, 8.0, 0.0)
(-4.0, 8.0, 0.0) -> (-9.0, 0.0, 0.0)
(-4.0, 8.0, 0.0) -> (-9.0, 0.0, 0.0)
(-4.0, 8.0, 0.0) -> (-9.0, 0.0, 0.0)
(4.0, -8.0, 0.0) -> (-9.0, 0.0, 0.0)
(4.0, 8.0, 0.0) -> (-4.0, 8.0, 0.0)
System is safe
